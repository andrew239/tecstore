import React, { useEffect, useState } from "react";
import { Error } from "./Error";
import * as R from 'ramda';
import {BookDetail} from './BookDetail';
import { BookDetailReviews } from "./BookDetailReviews";
export const SORT_BY = {
    RATING_DESC: 'Rating',
    ID_DESC: 'New',
};

export type sortType = 'RATING_DESC'| 'ID_DESC'

const query = `
fragment Book on Book{
    id
    title
    description
    imageUrl(size:LARGE)
    rating
}

fragment Review on Review{
    id
    rating
    title
    comment
    user{
      name
      imageUrl
    }
}

query BookPage($bookId:String!){
    book(bookId:$bookId){
        ...Book
        reviews{
           ...Review
        }
        authors{
            name
        }   
    }
}
`;

export function Book(props:any){
    const [book, setBook] = useState<any|null>(null);
    const [reviews, setReviews] = useState<any>([]);
    const [errors, setErrors] = useState<any>([]);
    const id = props.match.params.id;

    useEffect(()=>{
        (async()=>{
            const body = {
                query,
                variables:{bookId : id}
            }
            const queryRes = await fetch("http://localhost:4000/graphql",{
                method: "POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify(body)
            });
            const result = await queryRes.json();
            console.log(JSON.stringify(result));
            const newBook = R.path(['data','book'],result);
            const newReviews = R.path(['data','book','reviews'],result);
            console.log(newBook);
            setBook(newBook);
            setReviews(newReviews);
        })();
    },[]);

    return(<div>
      <div className="cf black-80 mv2">
        <Error errors={errors} />
        <BookDetail book={book} />
        <BookDetailReviews bookId={1} reviews={reviews} />
      </div>
    </div>);
}