export interface StockHistory{
    location:string,
    addedQuantity:number,
}

export interface Stock{
    location:string,
    quantity:number,
    histories:StockHistory[]
}

export interface InventoryHistory{
    stocks: Stock[]|undefined,
    stock: Stock | undefined,
    location: string,
    addedQuantity: number,
}

export interface Inventory {
    product: Food,
    location: string,
    quantity: number,
    remainQuantity: number,
    histories:InventoryHistory[],
}

export default interface Food {
    id: string,
    foodName: string,
    category: "DRINK" | "SNACK" | "NOODLES", //|string test only
    price: number,
    unit: "CAN" | "PACK" | "BOTTLE" | "CUP" | "BAG", //|string test only
    imageUrl: string
    stocks:Stock[] ,
    stock:Stock,
    inventories:Inventory[],
    inventory:Inventory,
    //salesVolumes:[]
}