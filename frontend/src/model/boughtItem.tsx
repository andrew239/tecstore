import Food from './food';
export default interface BoughtItem{
    food:Food,
    quantityInCart:number
}