import React from 'react';
import styles from './instruction.module.scss';

export default function Instruction(){
    return (<div className={styles.dndInstruction}>
        <div className={styles.dndInstructionBorder}>
          <span>Drag and Drop to Shopping Cart</span>
        </div>
      </div>);
}