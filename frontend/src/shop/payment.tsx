import React from 'react';
import {Link} from 'react-router-dom';
import styles from './payment.module.scss';

export default function Payment(props:any){
    const {totalPrices} = props;
    return (<div className={styles.paymentSection}>
        <div className={styles.paymentTitle}>
          <div className={styles.paymentTitleBackground}>
             <span>Payment</span>
          </div>
          <div className={styles.paymentContent}>
            <div className={styles.paymentPrice}>
                <span>HKD ${totalPrices}</span>
            </div>
            <div className={styles.paymentButton}>
                <Link to="/payment" className={styles.payButton}>
                  <span>Payment</span>
                </Link>
            </div>
          </div>
        </div>
      </div>);
}