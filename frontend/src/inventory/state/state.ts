import Food from '../../model/food';

export interface ITecStoreState{
    tecStore:Food[],
    prepareInventories:{
        [name:string]:{
            stockId:string,
            location:string,
            quantity:number
        }[]
    },
    isAuthenticated:boolean|undefined
}