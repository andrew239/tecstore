# Introduction
npm install bulma
yarn add ramda @types/ramda
yarn add i18next i18next-xhr-backend react-i18next
yarn add redux-thunk @types/redux-thunk


# References
https://www.freecodecamp.org/news/how-to-add-drag-and-drop-in-react-with-react-beautiful-dnd/
https://github.com/atlassian/react-beautiful-dnd
https://stackoverflow.com/questions/24224112/css-filter-make-color-image-with-transparency-white
https://csvjson.com/csv2json
http://jsfiddle.net/p2bWf/
https://stackoverflow.com/questions/12991351/css-force-image-resize-and-keep-aspect-ratio
https://stackoverflow.com/questions/14263594/how-to-show-text-on-image-when-hovering
https://www.npmjs.com/package/react-numeric-input
https://stackoverflow.com/questions/36113367/how-to-make-image-buttons-in-jsxs
https://stackoverflow.com/questions/37195590/how-can-i-persist-redux-state-tree-on-refresh
https://dev.to/jam3/managing-env-variables-for-provisional-builds-h37