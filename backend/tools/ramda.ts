//https://medium.com/d-d-mag/%E6%B7%BA%E6%9E%90%E5%B9%BE%E5%80%8B-ramda-%E7%95%B6%E4%B8%AD%E7%9A%84-api-c399a3f73c68
//https://marmelab.com/blog/2017/09/06/dive-into-graphql-part-iii-building-a-graphql-server-with-nodejs.html#splitting-resolvers
//https://stackoverflow.com/questions/31495239/title-case-a-sentence

import * as R from 'ramda';

 export const dataTransform = (datalist:any,key:any,value:any)=>{
    const transformFunc = R.groupBy((data)=>R.path([key],data) as any);
    return R.map((data:any)=>R.path([value],data[0]),transformFunc(datalist) as any);
}

export const stringifyItems = (data:any)=>R.map(item=>`('${item}')`,data);

export const stringifyItemsObjects = (rowData:any)=>R.map(data=>{
  return R.converge((value,key)=>{
    return R.reduce(
      (acc:any,item)=> R.indexOf(item,key) < R.length(key) - 1 ?
          acc.concat(`'${value[R.indexOf(item,key)]}',`) : 
          acc.concat(`'${value[R.indexOf(item,key)]}')`),
      '(',
      key
    );
  },[R.values,R.keys])(data);
},rowData);

export const dataMapping = (rowData:any,idMappingTable:any)=>{
    const mappingNameToId = (value:any,key:any,data:any)=>data[key]=R.pathOr(data[key],[key,data[key]],idMappingTable);
    return R.map(data=>R.forEachObjIndexed(mappingNameToId,data),rowData);
}

const keyValue = (value:any,key:any)=>value;
const keyValueAppender = (...elements:any)=>R.mergeAll(R.forEachObjIndexed(keyValue,elements));
const Header = (func:any,...data:any)=>{
    const f = R.partial(func,[data[0]]);
    const newData = R.drop(1,data);
    return {f:f,v:newData}; 
};
const body = ({f,v}:any)=>{
    const newV = R.drop(1,v); 
    if (newV.length == 0){
      return f(v[0]);
    }else{
      const func = R.partial(f,[v[0]]);
      return {f:func,v:newV};
    }
};

const Tail = R.curry((data/* Mapping Table! */)=>{
  return R.compose(
    R.compose(//2
      R.addIndex(R.map)((val:any,idx)=>val((v:any,key:any,item:any)=>{//2.1
        item[key] = R.pathOr(item[key],[key,item[key]],data);
      }))
    ),
    R.addIndex(R.map)((val,idx)=>{//1
      return R.forEachObjIndexed(R.__ as any,val);
    }),
    (x:any)=>x,
  )
})/*(Target List)*/;

// @ts-ignore
const dataMappingCreatorTemplate = (elementNum)=>R.compose(Tail,...R.repeat(body,elementNum-1),Header);
// @ts-ignore
let dataMappingCreator = R.curry((size,...args)=>dataMappingCreatorTemplate(size)(R.curryN(size,keyValueAppender),...args));
let reducerAppender = (func:any,x:any)=> x(func);
const reducerFunc = (elementNum:any,...args:any)=>R.transduce(
    R.map(R.curry(v=>R.partial(R.__ as any,[v]))), 
    reducerAppender,dataMappingCreator as any,
    [elementNum,...args]);
// @ts-ignore
const curriedReducerFunc = R.curry((...args)=>reducerFunc(...args));
const FuncFactory = (func:any,params:any = null)=>params?R.partial(func,[params]):R.partial(func,[]);
let tempFunc:any = null;
export const FuncBuilder = (param:any)=>{
  if (tempFunc){
    if (!Array.isArray(param)){
      tempFunc = FuncFactory(tempFunc,param);
      return;
    }else{
      const data = tempFunc(/* R.transduce() */)(/* dataMappingCreator() */)(param);
      tempFunc = null; //Reset and let it can be used next time
      return data;
    }
  }else{
    tempFunc = FuncFactory(curriedReducerFunc,param);
    return;
  }
};

export const extractValuesToArray = (data:any,fieldName:any)=>R.uniq(R.map(item=>item[fieldName],data));

export const mapDB_RecordByKeyToArrayInOrder = async (data:any,ids:any,key:string,defaultValue = [])=>{
  //Grouping data by id
  const rowsById = R.groupBy((record:any)=>record[key],data);
  //Mapping the records array by index order 
  return R.map((id:any)=>rowsById[id]?rowsById[id]:defaultValue,R.uniq(ids));
};

const aggSum = (key:any,data:any)=>R.sum(R.map(x=>x[key],data));
// export const transformDateFromFlattenToNestedStructure 
//       = async ( data: any,mappIds:any ,OuterKey: any,
//           InnerKey: any, transformDataFunc: any, 
//           aggKey: any, aggFunc = "sum" )=>{
//             const finalData = R.mapObjIndexed((outerItem: any) => {
//                 const innerItemsEachOuter = R.groupBy((innerElement: any) => innerElement[InnerKey], outerItem);
//                 if (aggFunc === "sum") {
//                   const innerItemsEachOuterInArr = R.map(id => transformDataFunc(id, innerItemsEachOuter, aggSum, aggKey), R.keys(innerItemsEachOuter));
//                   return innerItemsEachOuterInArr;
//                 } else {
//                   //TODO: implement other aggregation functions in future
//                   return;
//                 }
//               }, 
//               R.groupBy((outerElement: any) => outerElement[OuterKey], data));
//               return R.map(id=>finalData[id]?finalData[id]:[],mappIds);
// };
export const transformDateFromFlattenToNestedStructure = 
(data:any,mappIds:any,OuterKey:any,InnerKey:any,transformDataFunc:any,aggKey:any,aggFunc = aggSum )=>{ 
  const finalData = R.mapObjIndexed((outerItem:any) => {
  const innerItemsEachOuter = R.groupBy((innerElement:any) => innerElement[InnerKey], outerItem);
  const outerKeys = R.keys(innerItemsEachOuter);
  return R.map(y=>transformDataFunc(innerItemsEachOuter[y],aggKey,aggFunc),outerKeys);
},R.groupBy((outerElement:any) => outerElement[OuterKey], data));
  return R.map(id=>finalData[id]?finalData[id]:[],mappIds);
}

//R.flatten(R.map(f=>R.map(s=>s.location,f),data));
/*
const data = mapIndexed((first,idx)=>R.map(second=>{
    return {id:R.find(R.propEq('location',second))(resultsRaw[idx]).id,location:R.compose(R.toUpper,R.replace(" ","_"),R.replace(" Stock",""),R.identity)(second)}
},first),R.map(first=>R.map(second=>second.location,first),resultsRaw));

*/
const mapIndexed = R.addIndex(R.map);
export const convertFromDBValueToEnum = (data:any, end:string, delimiter:string,targetField:any)=>
R.flatten(R.map((f:any)=>R.map(s=>s[targetField],f),
  mapIndexed((first:any,idx:number)=>
  R.map((second:any)=>{
    return{ 
      id:(R.find(R.propEq(targetField,second))(data[idx]) as any).id,
      [targetField]:R.compose(R.toUpper,R.replace(delimiter,"_"),R.replace(delimiter,"_"),R.replace(end,""),R.identity)(second) 
    }
  },first),R.map(first=>R.map(second=>(second[targetField]),first),data))
));

const capitalizeWords = R.map((s:any)=> R.concat(R.toUpper(R.head(s)), R.toLower(R.tail(s))));
const toTitleCase = R.curry(word=>R.compose(R.join(' '), capitalizeWords, R.split('_'))(word));

export const convertEnumToDBValue = (rawData:any,key:any,replaces:any,mappingIdTable:any)=>R.compose(
  R.forEachObjIndexed((row:any)=>{
    // @ts-ignore
     row[key] = mappingIdTable[(R.compose(
        // @ts-ignore
        ...mapIndexed((val:any,idx:any)=>R.replace(val.i,val.r),replaces),
        // @ts-ignore
        toTitleCase,
        R.identity,
     )as any)(row[key])];//End of compose
     return;
    }
  )//End of forEachObjIndexed
)(rawData);

/*
const data1 = [
  { id: 1, location: 'Tsuen Wan 27/F' },
  { id: 2, location: 'Tsuen Wan 5/F' },
  { id: 3, location: 'Sheung Wan' }
];
===>
[
    {"Tsuen Wan 27/F": 1},
    {"Tsuen Wan 5/F": 2},
    {"Sheung Wan": 3}
]

*/
// @ts-ignore
export const extractValuesMappingToObj=(data:any)=>R.fromPairs(R.map(x=>(R.reverse(R.values(x))),data));
export const insertInBatchByString = (data:any)=>R.map(str=>{
  return  "(" + R.join(',', R.values(str)) + ")";
}, data);

export const groupDataBy = (key:any,data:any)=>R.groupBy((record:any)=>record[key],data);
export const insertPropToObj = (data:any,insertObj:any)=>R.map(elements=>R.mergeAll([{...elements},insertObj]),data);

const getPropsFromObjInMapCallbackFunc = 
R.curry((source:any,
 targetKey:any, 
 sourceKey:any,
 targetField:any,
 row:any)=> ({[targetField]:(R.find(R.propEq(targetKey, row[sourceKey])) as any)(source)[targetField]}) );

const elementHandler=(func:any)=>R.compose(
  (f:any)=>f(func),
  R.mergeRight(R.__)
);

export const getPropsFromObjInMapTemplate=(source:any,
  targetKey:any,
  sourceKey:any,
  targetField:any)=>getPropsFromObjInMapCallbackFunc(source,targetKey,sourceKey,targetField,R.__);

const merger=R.curry((func,data1)=>R.map(row=>elementHandler(func(row))(row),data1));
// @ts-ignore
const insertFieldsFuncCreator = (args)=>R.compose(...R.map((value,key)=>merger(value),[...args]));

export const insertFields = (funcs:any,data:any)=>(insertFieldsFuncCreator(funcs) as any)(data);
export const removeFields = (fieldName:any,data:any)=>R.map(e=>R.pickBy((val, key) => key !== fieldName,e),data);
export const removeFieldsInArray =  (fieldName:any,data:any)=>R.map(element=>removeFields(fieldName,element),data);

// @ts-ignore
export const replaceStringByList = (src:string,replaces:{i:string,r:string}[])=>(R.compose(
  ...mapIndexed((val:{i:string,r:string},idx:number)=>R.replace(val.i,val.r),replaces),
  toTitleCase,
  R.identity
) as any)(src);

