import Knex from 'knex';
import dotenv from 'dotenv';
import {DatabaseHelper} from './IDatabaseHelper';

const KnexConfig = require('../knexfile');
dotenv.config();
const knexConfig = KnexConfig[process.env.NODE_ENV || "development"];
const knex = Knex(knexConfig);

class KnexDatabase extends DatabaseHelper{
    constructor(private knex:any){
        super();
        this.knex = knex;
    }
    public database(){
        return this.knex;
    }

    public async raw(sql: string) {
        const result = await this.knex.raw(sql);
        return result.rows;
    }
}

const knexDB = new KnexDatabase(knex);
export default knexDB;