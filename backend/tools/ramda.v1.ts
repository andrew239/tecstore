import * as R from 'ramda';

/*
Definition of Curry
https://cythilya.github.io/2017/02/27/currying-in-javascript/
https://zh.wikipedia.org/wiki/%E6%9F%AF%E9%87%8C%E5%8C%96

References:
https://stackoverflow.com/questions/37040534/convert-array-of-objects-to-plain-object-using-ramda
https://fpjs.fun/ramda/function/chain/
https://www.itread01.com/articles/1489881448.html
*/

/*
const categoryList = ["DRINK", "SNACK","NOODLES"];
const result = [{"id":4},{"id":5},{"id":6}];
const valueMapping = R.map(data => data.id,result);
R.map(name=>({[name]:valueMapping[R.indexOf(name,categoryList)]}),categoryList);

[{"DRINK": 4}, {"SNACK": 5}, {"NOODLES": 6}]
 */
 
export const dataTransform = (datalist:any,key:any,value:any)=>{
    const transformFunc = R.groupBy((data)=>R.path([key],data) as any);
    /* transformFunc() will map following result
    {
        "DRINK": [{"category": "DRINK", "id": 7}], 
        "NOODLES": [{"category": "NOODLES", "id": 9}], 
        "SNACK": [{"category": "SNACK", "id": 8}]} 
    */
    return R.map((data:any)=>R.path([value],data[0]),transformFunc(datalist) as any);
    /* R.map will map following data structure
        {"DRINK": 7, "NOODLES": 9, "SNACK": 8}
    */
}

export const stringifyItems = (data:any)=>R.map(item=>`('${item}')`,data);

export const dataMapping = (rowData:any,idMappingTable:any)=>{
    const mappingNameToId = (value:any,key:any,data:any)=>data[key]=R.pathOr(data[key],[key,data[key]],idMappingTable);
    return R.map(data=>R.forEachObjIndexed(mappingNameToId,data),rowData);
}

const keyValue = (value:any,key:any)=>value;
const keyValueAppender = (...elements:any)=>R.mergeAll(R.forEachObjIndexed(keyValue,elements));
const Header = (func:any,...data:any)=>{
    const f = R.partial(func,[data[0]]);
    const newData = R.drop(1,data);
    return {f:f,v:newData}; 
};
const body = ({f,v}:any)=>{
    const newV = R.drop(1,v); 
    if (newV.length == 0){
      return f(v[0]);
    }else{
      const func = R.partial(f,[v[0]]);
      return {f:func,v:newV};
    }
}

const Tail = R.curry((data)=>{
  return R.compose(
    R.compose(//2
      R.addIndex(R.map)((val:any,idx)=>val((v:any,key:any,item:any)=>{//2.1
        item[key] = R.pathOr(item[key],[key,item[key]],data);
      }))
    ),
    R.addIndex(R.map)((val,idx)=>{//1
      return R.forEachObjIndexed(R.__ as any,val);
    }),
    (x:any)=>x,
  )
});

// @ts-ignore
const dataMappingCreatorTemplate = (elementNum)=>R.compose(Tail,...R.repeat(body,elementNum-1),Header);
// @ts-ignore
let dataMappingCreator = R.curry((size,...args)=>dataMappingCreatorTemplate(size)(R.curryN(size,keyValueAppender),...args));
//dataMappingCreator(3)(curriedAppender,{a:1},{b:2},{c:3})
/*R.compose(
  R.partial(__,[{c:1}]),
  R.partial(__,[{b:1}]),
  R.partial(__,[{a:1}]),
  R.partial(dataMappingCreator,__)
)([3])();*/
let reducerAppender = (func:any,x:any)=> x(func);
/*const a = R.transduce(
  R.map(R.curry(v=>R.partial(__,[v]))), 
  reducerAppender,dataMappingCreator,
  [3,{a:1},{b:1},{c:1}]);*/
const reducerFunc = (elementNum:any,...args:any)=>R.transduce(
    R.map(R.curry(v=>R.partial(R.__ as any,[v]))), 
    reducerAppender,dataMappingCreator as any,
    [elementNum,...args]);
//reducerFunc(3,{a:1},{b:1},{c:1})();
// @ts-ignore
const curriedReducerFunc = R.curry((...args)=>reducerFunc(...args));
const FuncFactory = (func:any,params:any = null)=>params?R.partial(func,[params]):R.partial(func,[]);
let tempFunc:any = null;
export const FuncBuilder = (param:any)=>{
  if (tempFunc){
    if (!Array.isArray(param)){
      tempFunc = FuncFactory(tempFunc,param);
      return;
    }else{      
      const data = FuncFactory(tempFunc()())(param);
      tempFunc = null; //Reset and let it can be used next time
      return data;
    }
  }else{
    tempFunc = FuncFactory(curriedReducerFunc,param);
    return;
  }
}
// const curriedMappingNameToId = R.curry((f:any)=> (value:any,key:any,data:any)=>data[key]=R.pathOr(data[key],[key,data[key]],f()()));
// export const FuncExecutor = (targetList:any)=>R.map(
//     data=>R.forEachObjIndexed(curriedMappingNameToId(tempFunc),data),
// targetList);

// export const FuncExecutor2 = R.compose(
//   R.compose(//2
//     R.addIndex(R.map)((val:any,idx)=>val((v:any,key:any,item:any)=>{//2.1
//       //foodName:40g卡樂B蝦條。原味
//       //console.log(`${k}:${v}`);
//       //->{"id":1,"foodName":"240ml四洲經典粒粒橙汁","category":"DRINK","price":5,"unit":"CAN","quantity":10,"imageUrl":"https://www.parknshop.com/medias/sys_master/front/prd/9314024751134.jpg","stocks":[],"inventories":[]}
//       //console.log("->" + JSON.stringify(d));
//       item[key] = R.pathOr(item[key],[key,item[key]],tempFunc()());
//     }))
//   ),
//   R.addIndex(R.map)((val,idx)=>{//1
//     return R.forEachObjIndexed(R.__ as any,val);
//   }),
//   (x:any)=>({...x})
// );

// export const FuncExecutor3 = R.compose(
//   R.compose(//2
//     //R.addIndex(R.map)(x=>console.log("f->"+x)),
//     R.addIndex(R.map)((val:any,idx)=>val((v:any,key:any,item:any)=>{//2.1
//       return R.pathOr(item[key],[key,item[key]],R.__);
//     }))
//   ),
//   R.addIndex(R.map)((val,idx)=>{//1
//     return R.forEachObjIndexed(R.__ as any,val);
//   }),
//   (x:any)=>({...x})
// );