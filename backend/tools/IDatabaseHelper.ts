export abstract class DatabaseHelper{
    public abstract database():void;
    public abstract raw(sql:string):any;
}