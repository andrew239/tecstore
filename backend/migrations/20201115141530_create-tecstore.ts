import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    //Item Section
    await knex.schema.createTable("category",(builder)=>{
        builder.increments();//id
        builder.string("category").notNullable();
    });

    await knex.schema.createTable("unit",(builder)=>{
        builder.increments();//id
        builder.string("unit").notNullable();
    })

    await knex.schema.createTable("item",(builder)=>{
        builder.increments();//id
        builder.string("food_name").notNullable();
        builder.integer("category").unsigned();
        builder.foreign("category").references("category.id");
        builder.double("price").defaultTo(0);
        builder.integer("unit").unsigned();
        builder.foreign("unit").references("unit.id");
        builder.text("image_url").notNullable();
    });

    //Stock Section
    await knex.schema.createTable("stock_location",(builder)=>{
        builder.increments();//id
        builder.string("location").notNullable();
    });  

    await knex.schema.createTable("stock",(builder)=>{
        builder.increments(); //id
        builder.integer("item").unsigned();
        builder.foreign("item").references("item.id");
        builder.integer("location").unsigned();
        builder.foreign("location").references("stock_location.id");
        builder.double("quantity").defaultTo(0);
        builder.timestamps(false,true);
    });

    //Inventory Section
    await knex.schema.createTable("inventory_location",(builder)=>{
        builder.increments();//id
        builder.string("location").notNullable();
    });
    
    await knex.schema.createTable("inventory",(builder)=>{
        builder.increments();//id
        builder.integer("item").unsigned();
        builder.foreign("item").references("item.id");
        builder.integer("location").unsigned();
        builder.foreign("location").references("inventory_location.id");
        builder.integer("stock").unsigned();
        builder.foreign("stock").references("stock_location.id");
        builder.double("quantity").defaultTo(0);
        builder.boolean("summarized").defaultTo(false);
        builder.boolean("writeoff").defaultTo(false);
        builder.timestamps(false,true);
    });

    //Transaction Section
    await knex.schema.createTable("payment_method",(builder)=>{
        builder.increments();//id
        builder.string("method").notNullable();
    });
    
    await knex.schema.createTable("approval_result",(builder)=>{
        builder.increments();//id
        builder.string("status").notNullable();
    });

    await knex.schema.createTable("bill",(builder)=>{
        builder.increments(); //id
        builder.double("total_price").defaultTo(0);
        builder.integer("payment_method").unsigned();
        builder.foreign("payment_method").references("payment_method.id");
        builder.timestamps(false,true);
        builder.integer("inventory").unsigned();
        builder.foreign("inventory").references("inventory.id");
        builder.string("credit_card_no").notNullable();
        builder.string("ref_no").notNullable();
        builder.integer("approval").unsigned();
        builder.foreign("approval").references("approval_result.id");
    });
    
    await knex.schema.createTable("transaction",(builder)=>{
        builder.increments();//id
        builder.integer("bill").unsigned();
        builder.foreign("bill").references("bill.id");
        builder.integer("item").unsigned();
        builder.foreign("item").references("item.id");
        builder.timestamps(false,true);
        builder.integer("inventory").unsigned();
        builder.foreign("inventory").references("inventory.id");
        builder.integer("quantity").defaultTo(0);
        builder.double("amount").defaultTo(0);
        builder.boolean("writeoff").defaultTo(false);
    });
}

export async function down(knex: Knex): Promise<void> {
    //Transaction Section
    await knex.schema.dropTableIfExists("transaction");
    await knex.schema.dropTableIfExists("bill");
    await knex.schema.dropTableIfExists("approval_result");
    await knex.schema.dropTableIfExists("payment_method");

    //Inventory Section
    await knex.schema.dropTableIfExists("inventory");
    await knex.schema.dropTableIfExists("inventory_location");

    //Stock Section
    await knex.schema.dropTableIfExists("stock");
    await knex.schema.dropTableIfExists("stock_location");

    //Item Section
    await knex.schema.dropTableIfExists("item");
    await knex.schema.dropTableIfExists("unit");
    await knex.schema.dropTableIfExists("category");
}

