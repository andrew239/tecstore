import { Router } from 'express';
import {createPaymentIntent} from '../services/StripeService';

class StripeRouter{
    router(){
        const router = Router();
        router.post('/create-payment-intent',createPaymentIntent)
        return router;
    }
}

export const stripeRouter = new StripeRouter();