import * as Knex from "knex";
import {defaultCategoryList,defaultUnitList,
    defaultFoodInfoList,stockLocationList,
    storeLocationList,paymentMethodList,approvalResultList} from '../data/data'
import { dataTransform ,stringifyItems,FuncBuilder,stringifyItemsObjects} from '../tools/ramda';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    //Transaction Section
    await knex("transaction").del();
    await knex("bill").del();
    await knex("approval_result").del();
    await knex("payment_method").del();
    
    //Inventory Section
    await knex("inventory").del();
    await knex("inventory_location").del();

    //Stock Section
    await knex("stock").del();
    await knex("stock_location").del();

    //Item Section
    await knex("item").del();
    await knex("unit").del();
    await knex("category").del();

    FuncBuilder(2);
    // Inserts seed entries
    const categoryList = stringifyItems(defaultCategoryList);
    const cResults =  await knex.raw(`insert into category (category) values ${categoryList.join(",")} returning category,id`);
    const category = dataTransform(cResults.rows,"category","id");
    FuncBuilder({category});

    const unitList = stringifyItems(defaultUnitList);
    const uResults = await knex.raw(`insert into unit (unit) values ${unitList.join(",")} returning unit,id`);
    const unit = dataTransform(uResults.rows,"unit","id");
    FuncBuilder({unit});
    const insertedData = FuncBuilder(defaultFoodInfoList);
    const itemList = stringifyItemsObjects(insertedData);
    await knex.raw(`insert into item (food_name,category,price,unit,image_url) values ${itemList.join(",")} returning id`);

    const stockLocList = stringifyItems(stockLocationList);
    await knex.raw(`insert into stock_location (location) values ${stockLocList.join(",")} returning location,id`);

    const storeLocList = stringifyItems(storeLocationList);
    await knex.raw(`insert into inventory_location (location) values ${storeLocList.join(",")} returning location,id`);

    const paymentMethList = stringifyItems(paymentMethodList);
    await knex.raw(`insert into payment_method (method) values ${paymentMethList.join(",")} returning method,id`);

    const approvalResList = stringifyItems(approvalResultList);
    await knex.raw(`insert into approval_result (status) values ${approvalResList.join(",")} returning status,id`);
};
