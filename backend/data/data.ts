export const defaultFoodInfoList = [
  {
    "foodName": "240ml四洲經典粒粒橙汁",
    "category": "DRINK",
    "price": 5,
    "unit": "CAN",
    "imageUrl": "240ml四洲經典粒粒橙汁.png"
  },
  {
    "foodName": "250ml維他-檸檬茶",
    "category": "DRINK",
    "price": 5,
    "unit": "PACK",
    "imageUrl": "250ml維他-檸檬茶.jpg"
  },
  {
    "foodName": "250ml維他-朱古力奶",
    "category": "DRINK",
    "price": 5,
    "unit": "PACK",
    "imageUrl": "250ml維他-朱古力奶.jpg"
  },
  {
    "foodName": "330ml(錫袋)利繽納",
    "category": "DRINK",
    "price": 7,
    "unit": "PACK",
    "imageUrl": "330ml(錫袋)利繽納.jpg"
  },
  {
    "foodName": "245ml (幼罐)寶礦力水特",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "245ml (幼罐)寶礦力水特.jpg"
  },
  {
    "foodName": "240ml(罐裝)S&W 芒果汁",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "240ml(罐裝)S_W 芒果汁.jpg"
  },
  {
    "foodName": "250ml雀巢咖啡。香滑",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "250ml雀巢咖啡。香滑.png"
  },
  {
    "foodName": "250ml雀巢咖啡。香濃",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "250ml雀巢咖啡。香濃.jpeg"
  },
  {
    "foodName": "315ml(罐裝)檸檬茶",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "315ml(罐裝)檸檬茶.jpg"
  },
  {
    "foodName": "330ml(罐裝)可口可樂 - ZERO",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "330ml(罐裝)可口可樂 - ZERO.jpg"
  },
  {
    "foodName": "330ml(罐裝)可口可樂 - 原味",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "330ml(罐裝)可口可樂 - 原味.jpg"
  },
  {
    "foodName": "330ml(罐裝) 玉泉忌廉",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "330ml(罐裝) 玉泉忌廉.jpg"
  },
  {
    "foodName": "330ml(罐裝)玉泉梳打水",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "330ml(罐裝)玉泉梳打水.jpeg"
  },
  {
    "foodName": "330ml葡萄適-原味",
    "category": "DRINK",
    "price": 7,
    "unit": "BOTTLE",
    "imageUrl": "330ml葡萄適-原味.jpg"
  },
  {
    "foodName": "420ml美粒果-果粒橙",
    "category": "DRINK",
    "price": 9,
    "unit": "BOTTLE",
    "imageUrl": "420ml美粒果-果粒橙.jpg"
  },
  {
    "foodName": "500ml津路烏龍茶",
    "category": "DRINK",
    "price": 9,
    "unit": "BOTTLE",
    "imageUrl": "500ml津路烏龍茶.png"
  },
  {
    "foodName": "500ml道地極品-解。綠茶",
    "category": "DRINK",
    "price": 9,
    "unit": "BOTTLE",
    "imageUrl": "500ml道地極品-解。綠茶.png"
  },
  {
    "foodName": "500ml CCLemon",
    "category": "DRINK",
    "price": 9,
    "unit": "BOTTLE",
    "imageUrl": "500ml CCLemon.png"
  },
  {
    "foodName": "330ml The Berry Company。藍莓汁",
    "category": "DRINK",
    "price": 15,
    "unit": "BOTTLE",
    "imageUrl": "330ml The Berry Company。藍莓汁.jpg"
  },
  {
    "foodName": "330ml The Berry Company。石榴汁",
    "category": "DRINK",
    "price": 15,
    "unit": "BOTTLE",
    "imageUrl": "330ml The Berry Company。石榴汁.jpg"
  },
  {
    "foodName": "250mlCoCoDay椰子水",
    "category": "DRINK",
    "price": 7,
    "unit": "BOTTLE",
    "imageUrl": "250mlCoCoDay椰子水.jpeg"
  },
  {
    "foodName": "330ml伯朗咖啡-特選咖啡",
    "category": "DRINK",
    "price": 7,
    "unit": "CAN",
    "imageUrl": "伯朗咖啡-特選咖啡.jpeg"
  },
  {
    "foodName": "媽咪杯。咖哩喇沙",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "媽咪杯。咖哩喇沙.jpg"
  },
  {
    "foodName": "日清出前一丁碗麵。麻油味",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "日清出前一丁碗麵。麻油味.jpg"
  },
  {
    "foodName": "日清出前一丁碗麵。豬骨味",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "日清出前一丁碗麵。豬骨味.jpg"
  },
  {
    "foodName": "日清出前一丁碗麵。韓辣味",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "日清出前一丁碗麵。韓辣味.jpg"
  },
  {
    "foodName": "合味道杯麵。咖喱",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "合味道杯麵。咖喱.jpg"
  },
  {
    "foodName": "合味道杯麵。五香牛肉",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "合味道杯麵。五香牛肉.jpg"
  },
  {
    "foodName": "合味道杯麵。海鮮",
    "category": "NOODLES",
    "price": 9,
    "unit": "CUP",
    "imageUrl": "合味道杯麵。海鮮.jpg"
  },
  {
    "foodName": "利是 夾心餅。芝士味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "利是 夾心餅。芝士味.jpg"
  },
  {
    "foodName": "Ovaltine朱古力夾心餅",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "Ovaltine朱古力夾心餅.jpg"
  },
  {
    "foodName": "Oreo夾心餅-原味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "Oreo夾心餅-原味.jpg"
  },
  {
    "foodName": "韓國烤紫菜。原味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "韓國烤紫菜。原味.jpg"
  },
  {
    "foodName": "韓國烤紫菜。芥末",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "韓國烤紫菜。芥末.jpg"
  },
  {
    "foodName": "迷你烤多士 - 蒜蓉味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "迷你烤多士 - 蒜蓉味.jpg"
  },
  {
    "foodName": "超味魷魚",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "超味魷魚.jpeg"
  },
  {
    "foodName": "樂天小熊餅-朱古力",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "樂天小熊餅-朱古力.jpeg"
  },
  {
    "foodName": "武平作蝦餅",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "武平作蝦餅.jpeg"
  },
  {
    "foodName": "6gYaokin美味棒。忌廉湯味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "6gYaokin美味棒。忌廉湯味.jpg"
  },
  {
    "foodName": "6gYaokin美味棒。芝士味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "6gYaokin美味棒。芝士味.jpg"
  },
  {
    "foodName": "固力果百奇餅乾條-沙律",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "固力果百奇餅乾條-沙律.jpg"
  },
  {
    "foodName": "固力果百奇餅乾條-草莓",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "固力果百奇餅乾條-草莓.jpg"
  },
  {
    "foodName": "31.8g 多樂脆粟米片。芝士",
    "category": "SNACK",
    "price": 5,
    "unit": "BAG",
    "imageUrl": "31.8g 多樂脆粟米片。芝士.jpg"
  },
  {
    "foodName": "25g卡樂B薯片-熱浪",
    "category": "SNACK",
    "price": 5,
    "unit": "BAG",
    "imageUrl": "25g卡樂B薯片-熱浪.jpg"
  },
  {
    "foodName": "百邦迷你條裝-清湯味薯片",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "百邦迷你條裝-清湯味薯片.jpg"
  },
  {
    "foodName": "35gWIZARD醒目牌系列。淮鹽花生",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "35gWIZARD醒目牌系列。淮鹽花生.jpeg"
  },
  {
    "foodName": "14.2g藍鑽石杏仁-蜜糖焗杏仁",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "14.2g藍鑽石杏仁-蜜糖焗杏仁.jpg"
  },
  {
    "foodName": "(盒裝)泰國固力果百力滋-香辣燒烤",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "(盒裝)泰國固力果百力滋-香辣燒烤.jpg"
  },
  {
    "foodName": "21g(掛裝)旺旺黑白配",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "21g(掛裝)旺旺黑白配.jpg"
  },
  {
    "foodName": "40g卡樂B蝦條。原味",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "40g卡樂B蝦條。原味.jpg"
  },
  {
    "foodName": "20gMEKO一口脆魚乾。芥末味",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "20gMEKO一口脆魚乾。芥末味.jpg"
  },
  {
    "foodName": "20gMEKO一口脆魚乾。冬蔭功味",
    "category": "SNACK",
    "price": 7,
    "unit": "BAG",
    "imageUrl": "20gMEKO一口脆魚乾。冬蔭功味.jpg"
  },
  {
    "foodName": "40g(包裝)M&Ms_朱古力-花生",
    "category": "SNACK",
    "price": 8,
    "unit": "BAG",
    "imageUrl": "40g(包裝)M_M_s朱古力-花生.jpg"
  },
  {
    "foodName": "(罐裝)53g品客薯片-忌廉洋蔥",
    "category": "SNACK",
    "price": 9,
    "unit": "CAN",
    "imageUrl": "(罐裝)53g品客薯片-忌廉洋蔥.jpg"
  },
  {
    "foodName": "55g卡樂B薯片。薄餅",
    "category": "SNACK",
    "price": 9,
    "unit": "BAG",
    "imageUrl": "55g卡樂B薯片。薄餅.jpg"
  },
  {
    "foodName": "110g德國橡皮軟糖。草莓士的",
    "category": "SNACK",
    "price": 10,
    "unit": "BAG",
    "imageUrl": "110g德國橡皮軟糖。草莓士的.jpg"
  },
  {
    "foodName": "110g德國橡皮軟糖。酸砂可樂",
    "category": "SNACK",
    "price": 10,
    "unit": "BAG",
    "imageUrl": "110g德國橡皮軟糖。酸砂可樂.jpg"
  },
  {
    "foodName": "益達無糖香口珠",
    "category": "SNACK",
    "price": 10,
    "unit": "BAG",
    "imageUrl": "益達無糖香口珠.jpg"
  },
  {
    "foodName": "(迷你包裝)HARIBO橡皮糖。迷你熊仔",
    "category": "SNACK",
    "price": 2,
    "unit": "BAG",
    "imageUrl": "(迷你包裝)HARIBO橡皮糖。迷你熊仔.jpeg"
  },
  {
    "foodName": "41.5gKitKat(4指)朱古力",
    "category": "SNACK",
    "price": 8,
    "unit": "BAG",
    "imageUrl": "KitKat(4指)朱古力.png"
  },
  {
    "foodName": "旺旺-雪餅",
    "category": "SNACK",
    "price": 2,
    "unit": "BAG",
    "imageUrl": "旺旺-雪餅.jpg"
  },
  {
    "foodName": "57g森永軟糖-提子",
    "category": "SNACK",
    "price": 6,
    "unit": "BAG",
    "imageUrl": "森永軟糖-提子.jpg"
  },
  {
    "foodName": "桂格燕麥曲奇-提子",
    "category": "SNACK",
    "price": 4,
    "unit": "BAG",
    "imageUrl": "桂格燕麥曲奇-提子.jpeg"
  },
  {
    "foodName": "麥維他消化餅條-原味",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "麥維他消化餅條-原味.jpg"
  },
  {
    "foodName": "小老闆紫菜。原味",
    "category": "SNACK",
    "price": 2,
    "unit": "BAG",
    "imageUrl": "小老闆紫菜。原味.jpeg"
  },
  {
    "foodName": "利是夾心餅。檸檬",
    "category": "SNACK",
    "price": 3,
    "unit": "BAG",
    "imageUrl": "利是夾心餅。檸檬.png"
  }
];

export const storeLocationList = [
    "Tsuen Wan 27/F",
    "Tsuen Wan 5/F",
    "Sheung Wan"
];

export const stockLocationList = [
    "Sheung Wan Stock",
    "Tsuen Wan Stock"
];

export const defaultCategoryList = ["DRINK", "SNACK","NOODLES"];
export const defaultUnitList = ["CAN","BOTTLE","CUP","BAG","PACK"];
export const paymentMethodList = ["CREDIT_CARD","PAYME","CASH"];
export const approvalResultList = ["APPROVED","REJECTED"];