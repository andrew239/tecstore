import {Request,Response} from 'express';
import {Stripe} from 'stripe';
import * as dotenv from 'dotenv';
dotenv.config();

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY as string,{
    apiVersion: '2020-08-27',
});

const mapping = {
    "Tsuen Wan 27/F":process.env.STRIPE_TW27_STORE,
    "Tsuen Wan 5/F":process.env.STRIPE_TW5_STORE,
    "Sheung Wan":process.env.STRIPE_SW_STORE
}

export const createPaymentIntent = async (req:Request, res:Response)=>{
    try{
    const {totalPrice,location,shoppingCart} = req.body;
    const cart = shoppingCart.map((item:any)=>item.food.foodName + "\t\t"+ item.quantityInCart);
    const paymentIntent = await stripe.paymentIntents.create({
        amount: parseInt(totalPrice),
        currency: "hkd",
        description: `TecStore@${location}`,
        customer: mapping[location],
        metadata: {
            shoppingCart: cart.join('\n')
        },
    });
    return res.json({clientSecret:paymentIntent.client_secret});
}catch(ex){
    return res.json({error:ex})
}
}