/*
References:
https://softchris.github.io/pages/graphql-express.html#parameterized-query
https://github.com/softchris/graphql-graphiql/blob/master/app.js
https://itnext.io/building-your-first-graphql-server-d5c4f88f5e82
 */
/*Server Part*/
import Express ,{Request,Response} from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
//import {stripeRouter} from './routers/stripeRouter';
//GraphQL Part
/*
import some GraphQL-related helper functions as well as the GraphQL schema describing our data model.
*/
import {graphqlHTTP} from 'express-graphql';
import typeDefs from './schema/schema';
import resolvers from './resolver/resolver';
import {makeExecutableSchema} from 'graphql-tools';
import { graphqlUploadExpress } from 'graphql-upload';
import {context} from './resolver/common/context';

import dotenv from 'dotenv';
dotenv.config();

//import { ApolloServer } from 'apollo-server-express'; 

//(async () => {
    /* - Server Part: Start -*/
    const PORT = 4000;
    const app = Express();
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    //const server = new ApolloServer({typeDefs,resolvers,context});
    app.use(cors());
    //app.use("/stripe",stripeRouter.router());
    app.use(Express.static("./upload"));
    /* - Server Part: End -*/

    /* - GraphQL Part: Start -*/
    const schema = makeExecutableSchema({ typeDefs, resolvers });
    app.use('/graphql',
        graphqlUploadExpress({ maxFileSize: 10000000 /*10MB*/, maxFiles: 10 }) ,
        graphqlHTTP({
        schema,
        //rootValue:resolver,
        graphiql: !process.env.DISABLE_GQL_PG?true:false,
        context
    }));
    //server.applyMiddleware({app,path:'/graphql'}); 
    /* - GraphQL Part: End -*/


    /* - Server Part: Start -*/
    app.post("/login",(req:Request, res:Response)=>{
        if (req.body.username === "admin" && req.body.password === "pmDjBamdZBmuD9zKD8dr7ZEk4bsYsxWsurAFrLv2"){
            return res.json({success:true});
        }
        return res.json({success:false});
    });

    app.listen(PORT, () => {
        console.log(`GraphQL Server is started at ${PORT}. The URL is https://localhost:${PORT}/graphql`);
    });
    /* - Server Part: End -*/
//})();