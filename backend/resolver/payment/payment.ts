/*
https://stackoverflow.com/questions/34415987/stripe-payment-getting-error-as-customer-cus-does-not-have-a-linked-card
https://www.pluralsight.com/guides/how-to-integrate-stripe-with-react

*/
import {ITransaction} from '../transaction/transaction';
import {Stripe} from 'stripe';
import * as dotenv from 'dotenv';
import {extractValuesToArray,groupDataBy,insertInBatchByString,removeFields} from '../../tools/ramda';
import knex from '../../tools/KnexDatabaseHelper';
import {extractValuesMappingToObj,getPropsFromObjInMapTemplate,insertFields} from '../../tools/ramda';
import {getCashRecordId} from '../products/products';
import moment from 'moment';
dotenv.config();

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY as string,{
    apiVersion: '2020-08-27',
});

export interface AddPaymentRecordInput{
    storeLocation:string,
    productId:number,
    quantity:number
}

export interface IBill{
    id:number,
    totalPrice:number,
    paymentMethod:string,
    creditCardNo:string,
    date:number,
    refNo:string,
    approval:string,
    records: ITransaction[]
}

async function getPaymentMethodMappingTable() {
    return await knex.raw(`select * from payment_method`);    
}

async function getApprovalResultMappingTable() {
    return await knex.raw(`select * from approval_result`);
}

export async function payment(paymentMethod:any,totalPrice:any,shoppingCart:AddPaymentRecordInput[],tokenId:string|undefined,storeLocation:number){
    if (paymentMethod === "CREDIT_CARD"){
        //Insert Bill Record
        const ids = extractValuesToArray(shoppingCart,"productId");
        const products = await knex.raw(`select id as item,food_name ,price from item where id = ANY('{${ids.join(",")}}')`);
        const productNameIdMapping = await groupDataBy("item",products);
        const cart = shoppingCart.map((item:any)=>productNameIdMapping[item.productId][0].food_name + "\t\t"+ item.quantity);
        const location = shoppingCart[0].storeLocation;
        
        const result = await stripe.charges.create({
            amount:totalPrice,
            currency:"hkd",
            source:tokenId,
            description: `TecStore@${location}`,
            metadata: {
                shoppingCart: cart.join('\n')
            },
            statement_descriptor:location
        });
    
        const allPaymentMethods = await getPaymentMethodMappingTable();
        const allPaymentMethodInIds = await extractValuesMappingToObj(allPaymentMethods);
        
        const allApprovalStatus = await getApprovalResultMappingTable();
        const allApprovalStatusInIds = await extractValuesMappingToObj(allApprovalStatus);

        //Sample SQL: insert into bill (total_price,payment_method,credit_card_no,ref_no,approval) values (10.0,1,4242,1608398454,1);
        const billId = await knex.raw(`insert into bill (total_price,payment_method,credit_card_no,ref_no,approval,inventory) values (
            ${totalPrice},
            ${allPaymentMethodInIds[paymentMethod]},
            ${result.payment_method_details?.card?.last4},
            ${result.created},
            ${allApprovalStatusInIds[result.outcome?.network_status?.toUpperCase().replace("_BY_NETWORK","") as string]},
            ${storeLocation}) returning id`);
        
        //Insert Transaction Records
        const constantTemplate=(row:any)=>({bill:billId[0].id,inventory:storeLocation});
        const getPropsFromObjInMap = getPropsFromObjInMapTemplate(shoppingCart,"productId","item","quantity");
        const transactionRecords = removeFields("food_name", 
            insertFields([getPropsFromObjInMap,constantTemplate],products));
        await knex.raw(`insert into transaction (item,amount,bill,inventory,quantity) values 
        ${insertInBatchByString(transactionRecords).join(',')}`);

        return {
            id:billId[0].id,
            totalPrice:totalPrice,
            paymentMethod:paymentMethod,
            creditCardNo:result.payment_method_details?.card?.last4,
            date:result.created,
            refNo:result.created,
            approval:result.outcome?.network_status?.toUpperCase().replace("_BY_NETWORK","")
        }
    }else if(paymentMethod === "CASH"){
        const allPaymentMethods = await getPaymentMethodMappingTable();
        const allPaymentMethodInIds = await extractValuesMappingToObj(allPaymentMethods);

        const allApprovalStatus = await getApprovalResultMappingTable();
        const allApprovalStatusInIds = await extractValuesMappingToObj(allApprovalStatus);
        
        const itemId = await getCashRecordId();
        const created = moment(tokenId+"-01","YYYY-MM-DD");

        const billId = await knex.raw(`insert into bill (total_price,payment_method,credit_card_no,ref_no,approval,inventory) values (
            ${totalPrice},
            ${allPaymentMethodInIds[paymentMethod]},
            '0000',
            ${created.unix().toString()},
            ${allApprovalStatusInIds["APPROVED"]},
            ${storeLocation}) returning id`);
            await knex.raw(`insert into transaction (item,amount,bill,inventory,quantity) values (${itemId},${totalPrice},${billId[0].id},${storeLocation},0)`);

            return {
                id:billId[0].id,
                totalPrice:totalPrice,
                paymentMethod:paymentMethod,
                creditCardNo:"0000",
                date:created.unix(),
                refNo:created.unix().toString(),
                approval:"APPROVED"
            }
    } else {
        //TODO: TBI in the future if available
        return {};
    }
}