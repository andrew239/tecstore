import knex from '../../tools/KnexDatabaseHelper';
import {extractValuesToArray,mapDB_RecordByKeyToArrayInOrder,convertFromDBValueToEnum} from '../../tools/ramda';
import * as changeCase from "change-case";
import humps from 'humps';
import DataLoader from 'dataloader';

export interface IAddStockRecordInputMetadata{
    stockLocation:string,
    productId:number,
    quantity:number
}
export interface IAddStockRecordInput{
    record:IAddStockRecordInputMetadata
}

export async function getAllStockLocation(){
    const results = await knex.raw(`select * from stock_location`);
    return results.map((d:any)=>{
        return {
            id:d.id,
            location:(d.location as string).replace(" Stock","")
        }
    })
  }

//This is for explanation of the advantage of dataloader, demo purpose
export async function getStockLocationNameById(id:number) {
    const result = await knex.raw(`select location from stock_location where id = ${id}`);
    return extractValuesToArray(result,"location")[0].replace("Stock","").replace(" ","_").replace(" ","").toUpperCase();
}

export async function getStockLocationNameByIds(ids:any) {
    const results = await knex.raw(`select * from stock_location where id = ANY('{${ids}}')`);
    const resultsRaw = await mapDB_RecordByKeyToArrayInOrder(results,ids,"id"); 
    return convertFromDBValueToEnum(resultsRaw," Stock"," ","location");
}

export function stocksLocationNameLoader(){
    return new DataLoader(getStockLocationNameByIds);
}

export async function getStockLocationId(name:string) {
    const location = changeCase.capitalCase(name.replace("_"," ").toLowerCase());
    const result = await knex.raw(`select id from stock_location where location like '${location}%'`);
    return extractValuesToArray(result,"id")[0];
}

export async function addStockRecord(record:IAddStockRecordInputMetadata) {
    const stock_location_id = await getStockLocationId(record.stockLocation);
    const result = await knex.raw(`insert into stock (item,location,quantity) values (${record.productId},${stock_location_id},${record.quantity}) returning id,item,location,quantity`);
    return humps.camelizeKeys(result[0]);
}

// export function stocksByProductIdsLoader(){
//     return new DataLoader(getStocksByProductIds);
// }

// const transformFunc = (element:any,key:any,aggFunc:any)=>({
//     item:element[0].item,
//     location:element[0].location,
//     quantity:aggFunc(key,element),
//     histories:element.map((i:any)=>({
//       location:i.location,
//       quantity:i.quantity
//     }))
//  }) 

// export async function getStocksByProductIds(ids:any) {
//     const results = await knex.raw(`select item,location,quantity from stock where item = ANY('{${ids.length>1?ids.join(','):ids[0]}}')`);
//     const tranformedResults = await transformDateFromFlattenToNestedStructure(results,ids,"item","location",transformFunc,"quantity");
//     return tranformedResults;
// }

export function stocksByIdsLoader() {
    return new DataLoader(getStocksByIds);
}

export async function getStocksByIds(ids:any) {
    const results = await knex.raw(`select * from stock where id = ANY('{${ids}}')`);
    const resultsRaw = await mapDB_RecordByKeyToArrayInOrder(results,ids,"id");
    return resultsRaw;
}

export function stocksByProductIdLoader(){
    return new DataLoader(getStocksByProductId);
}

export async function getStocksByProductId(ids:any){
    const results = await knex.raw(`select id, item,location,quantity from stock where id = ANY('{${ids.length>1?ids.join(','):ids[0]}}')`);
    const productIds = extractValuesToArray(results,"item");
    const locationIds = extractValuesToArray(results,"location");
    const histories = await knex.raw(`select ref.id as stock_id,histories.id as id,histories.item as item,histories.location as location,histories.quantity as quantity from (select * from stock where id = ANY('{${ids.length>1?ids.join(','):ids[0]}}')) as ref inner join (select id,item,location,quantity from stock where item = ANY('{${productIds.length>1?productIds.join(','):productIds[0]}}')) as histories on ref.item = histories.item and ref.location = histories.location`);
    const historiesById = await mapDB_RecordByKeyToArrayInOrder(histories,ids,"stock_id");
    let  historiesByItemAndLocation = [];
    for(const stockRecordsByStockId of historiesById){
        let stockRecordsByLocationArray = {};
        for(const loc of locationIds){
            const stockRecordsByLocation =  await mapDB_RecordByKeyToArrayInOrder(
              stockRecordsByStockId.filter(ele=>ele.location === loc),[loc],"location");
              stockRecordsByLocationArray = {...stockRecordsByLocationArray,[loc]:stockRecordsByLocation[0]};
        }
        historiesByItemAndLocation.push(stockRecordsByLocationArray);
    }
    return historiesByItemAndLocation;
}

export function sumOfStockQtyByProductLoader(){
    return new DataLoader(sumOfStockQtyByProduct);
}

export async function sumOfStockQtyByProduct(ids:any) {
    const results = await knex.raw(`select distinct item from stock where id = ANY('{${ids.length>1?ids.join(','):ids[0]}}')`);
    const productIds = extractValuesToArray(results,"item");
    //const finalSum = await knex.raw(`select stock.id as id ,sum as quantity from stock inner join (select item,location,sum(quantity) from stock where item = ANY('{${productIds.length>1?productIds.join(','):productIds[0]}}') group by item,location) as sumValues on stock.item = sumValues.item and stock.location = sumValues.location`);
    const finalSum = await knex.raw(`Select leftTable.id, (leftTable.quantity - cast(coalesce(rightTable.quantity,0) as integer)) as quantity from (select stock.id as id,stock.item,stock.location,sum as quantity from stock inner join (select item,location,sum(quantity) from stock where item = ANY('{${productIds.length>1?productIds.join(','):productIds[0]}}') group by item,location) as sumValues on stock.item = sumValues.item and stock.location = sumValues.location) as leftTable left outer join (select item,stock, sum(quantity) as quantity from inventory where item = ANY('{${productIds.length>1?productIds.join(','):productIds[0]}}') and writeoff = false group by item,stock) as rightTable on leftTable.item = rightTable.item and leftTable.location = rightTable.stock`);
    const f = await mapDB_RecordByKeyToArrayInOrder(finalSum,ids,"id");
    return f.map(d=>d[0]);
}

export async function getStockIdsByProduct(productId:number){
    return await knex.raw(`select id from stock where item = ${productId}`);
}

export async function getLatestStockIdByProduct(productId:number){
    return await knex.raw(`select location, max(id) as id from stock where item=${productId} group by location`);
}

export function getLatestStockIdsByLocationLoader(){
    return new DataLoader(getLatestStockIdsByLocation);
}

export async function getLatestStockIdsByLocation(productIds:any) {
    const ids = await knex.raw(`select location,item,max(id) as id from stock where item= ANY('{${productIds}}') group by location,item`);
    const results = await mapDB_RecordByKeyToArrayInOrder(ids,productIds,"item");
    return results;
}