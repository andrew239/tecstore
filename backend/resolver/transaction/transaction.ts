import DataLoader from 'dataloader';
import knex from '../../tools/KnexDatabaseHelper';
import {mapDB_RecordByKeyToArrayInOrder} from '../../tools/ramda';

export interface ITransaction{
    id:number,
    bill:number,
    product:number,
    quantity:number,
    amount:number
}

export function transactionsByBillIdDataLoader() {
    return new DataLoader(getTransactionsByIds);
}

async function getTransactionsByIds(ids:any) {
    const results = await knex.raw(`select id,bill,item as product,quantity,amount from transaction where bill=ANY('{${
        ids.length > 1?ids.join(","):ids[0]
    }}')`);
    return await mapDB_RecordByKeyToArrayInOrder(results,ids,"bill");
}