import {
    IProduct,getProductById,getAllOfProductsIds,getProductFieldFromArray,/*IProductInput,*/addItem
} from './products/products';
import { IAddStockRecordInput, addStockRecord, getStockLocationId} from './stock/stock';
import { IAddInventoryInput, addInventoryRecord,
    getInventoryFieldFromArray,groupInventoryHistoriesByLocation,getInventoryLocationId,stockTake} from './inventory/inventory';
import { IBill } from './payment/payment';
import { payment } from './payment/payment';
import { ITransaction } from './transaction/transaction';
import dotenv from 'dotenv';
import moment from 'moment';
dotenv.config();

// Custom Scalar Type
// https://ithelp.ithome.com.tw/articles/10206366
import { GraphQLScalarType, Kind } from 'graphql';
import { GraphQLUpload } from 'graphql-upload';

const resolvers = {
    //Custom Scalat Type
    Date: new GraphQLScalarType({
        name: 'Date',
        description: 'Date custom scalar type. Please input the date in format MMDDYYYY HHmmss, for example, 12202020 141000, in string',
        serialize(value) {
            //Value sent to the client
            return moment.unix(value).format("DD-MM-YYYY hh:mm:ss A");
        },
        parseValue(value) {
            //value from the client (variables)
            return moment(value, "MMDDYYYY HHmmss");
        },
        parseLiteral(ast) {
            //value from the client (inline arguments)
            if (ast.kind === Kind.STRING) {
                return moment(ast.value, "MMDDYYYY HHmmss")
            }
            return null;
        }
    }),
    Upload:GraphQLUpload,
    //type level
    Query: {
        //field level
        product: async (_: any, { id }: any,context:any) => {
            const {loaders:{getProductsByIdLoader}} = context;
            return await getProductsByIdLoader.load(id);
        },
        products: async (_:any,args:any,context:any) =>{
            const ids = await getAllOfProductsIds();
            const {loaders:{getProductsByIdLoader}} = context;
            return await getProductsByIdLoader.loadMany(ids);
        },
        productsByCategory: async (_: any, { category }: any, context: any) => {
            const { loaders:{productsByCategory} } = context;
            const { loaders:{getCategoryIdsLoader} } = context;
            const categories = await getCategoryIdsLoader.load(category);
            return await productsByCategory.load(categories[0].id);
        }
    },

    Stock: {
        location: async (parent: any, _: any, context: any) => {
            // const { loaders } = context;
            // const { getStocksLocationNameLoader } = loaders;
            // return await getStocksLocationNameLoader.load(parent.location);
            // This is for explanation of the advantage of dataloader, demo purpose
            //return getStockLocationNameById(parent.location);
            const {loaders:{getStocksByIds}} = context;
            const result = await getStocksByIds.load(parent.id);
            //return getStockLocationNameById(result[0].location);
            const {loaders:{getStocksLocationNameLoader}} = context;
            return await getStocksLocationNameLoader.load(result[0].location);
        },
        quantity: async (parent:any,_:any,context:any)=>{
            const {loaders:{sumOfStockQtyByProduct}} = context;
            const result =  await sumOfStockQtyByProduct.load(parent.id);
            return result.quantity;
        },
        histories: async (parent:any,_:any,context:any)=>{
           const {loaders:{getStocksByProductId}} = context;
           const result = await getStocksByProductId.load(parent.id);//Stock Record Id
           return result[parent.location];
        }
    },
    StockHistory: {
        location: async (parent: any, _: any, context: any) => {
            const { loaders } = context;
            const { getStocksLocationNameLoader } = loaders;
            return await getStocksLocationNameLoader.load(parent.location);
            // This is for explanation of the advantage of dataloader, demo purpose
            //return getStockLocationNameById(parent.location);
        },
        addedQuantity: (root: any) => root.quantity
    },
    Inventory: {
        product:async(parent:any,_:any,context:any)=>{
            const {loaders:{getProductsByIdLoader}} = context;
            return await getProductsByIdLoader.load(parent[0].item);
        },
        quantity:(parent:any)=>parent[0].quantity,
        location: async (parent: any, _: any, context: any) => {
            const { loaders } = context;
            const { getInventoryLocationNameLoader } = loaders;
            return await getInventoryLocationNameLoader.load(parent[0].location);
        },
        remainQuantity:async (parent:any,_:any,context:any)=>{
            //console.log(JSON.stringify({item:parent[0].item,location:parent[0].location}));
            const {loaders:{updatingProductRemainingQuantityLoader}} = context;
            const f = await updatingProductRemainingQuantityLoader.load(
                JSON.stringify({item:parent[0].item,location:parent[0].location})
            );
            return (Object.values(f)[0] as any).remain;
        },
        histories: async(parent:any,_:any,context:any) =>{
            //Update the inventory histories
            const {loaders:{getInventoryHistoriesByInventoryIdsLoader}} = context;
            const t = await getInventoryHistoriesByInventoryIdsLoader.load(parent[0].id);
            const data = groupInventoryHistoriesByLocation(t);
            return data[parent[0].location];
        }
    },
    InventoryHistory: {
        stocks: async (parent:any,_:any,context:any)=>{
            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const histories = await getLatestStockIdsByLocationLoader.load(parent.item);
            const historiesByLocation = histories.filter((item:any)=>item.location === parent.location);
            return historiesByLocation?historiesByLocation:[];
        },
        stock: async (parent:any,args:any,context:any)=>{
            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const histories = await getLatestStockIdsByLocationLoader.load(parent.item);
            const stockId = parent.location;//await getStockLocationId(args.location);
            const historiesByLocation = histories.filter((item:any)=>item.location === stockId);
            return historiesByLocation?historiesByLocation:[];
        },
        location: async (parent: any, _: any, context: any) => {
            const { loaders } = context;
            const { getInventoryLocationNameLoader } = loaders;
            return await getInventoryLocationNameLoader.load(parent.location);
        },
        addedQuantity: (root: any) => root.quantity
    },
    Product: {
        category: async (root: IProduct,_:any,context:any) => {
            const {loaders:{getProductCategoryNameLoader}} = context;
            const f = await getProductCategoryNameLoader.load(root.category);
            return getProductFieldFromArray(f,"category")[0];
        },
        unit: async (root: IProduct,_:any,context:any) => {
            const {loaders:{getUnitNameByIdsDataLoader}} = context;
            const f = await getUnitNameByIdsDataLoader.load(root.unit);
            return getProductFieldFromArray(f,"unit")[0];
        },
        //Remark: Case Sensitive, e.g., imageURLx imageUrl/
        imageUrl: (root: IProduct) => process.env.BACKEND_URL + "/" + root.imageUrl,
        stocks:async (root:IProduct,_:any,context:any)=>{
            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const f = await getLatestStockIdsByLocationLoader.load(root.id);
            return f;
        },
        stock: async (root:IProduct,args:any,context:any)=>{
            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const histories = await getLatestStockIdsByLocationLoader.load(root.id);
            const stockId = await getStockLocationId(args.location);
            const historiesByLocation = histories.filter((item:any)=>item.location === stockId);
            return historiesByLocation?historiesByLocation:[];
        },
        inventories: async (root: IProduct, _: any, context: any) => {
            const {loaders:{getLatestInventoriesIdsByProductAndLocationLoader}} = context;
            const inventoryIdsByLocation = await getLatestInventoriesIdsByProductAndLocationLoader.load(root.id);
            const {loaders:{getInventoryByIdsLoader}} = context;
            const results = await getInventoryByIdsLoader.loadMany(getInventoryFieldFromArray(inventoryIdsByLocation,"id"));
            return results;
        },
        inventory: async (root:IProduct,args:any,context: any) =>{
            const {loaders:{getLatestInventoriesIdsByProductAndLocationLoader}} = context;
            const inventoryIdsByLocation = await getLatestInventoriesIdsByProductAndLocationLoader.load(root.id);
            const {loaders:{getInventoryByIdsLoader}} = context;
            const results = await getInventoryByIdsLoader.loadMany(getInventoryFieldFromArray(inventoryIdsByLocation,"id"));
            const location = await getInventoryLocationId(args.location);
            let list:any = [];
            for(const inv of results){
                const filteredResults = inv.filter((d:any)=>d.location === location);
                for(const filteredResult of filteredResults){
                    list.push(filteredResult);
                }
            }
            return list.length>0?list:undefined;
        }
    },
    Bill:{
        records: async(parent:IBill,_:any,context:any)=>{
            const {loaders:{getTransactionsByBillIdDataLoader}} = context;
            return await getTransactionsByBillIdDataLoader.load(parent.id);
        }
    },
    Transaction:{
        product:(parent:ITransaction)=>getProductById(parent.product)
    },
    Mutation: {
        payment: async (_: any, { paymentMethod, totalAmount, records, tokenId }: any,context:any) => {
            const {loaders:{inventoryLocationNameWithConvertionLoader}} = context;
            const storeLocation = await inventoryLocationNameWithConvertionLoader.load(records[0].storeLocation);

            const { loaders: { updatingProductRemainingQuantityLoader } } = context;
            for (const { productId } of records) {
                //console.log(JSON.stringify({item:productId,location:storeLocation[0].storeLocation}));
                updatingProductRemainingQuantityLoader.clear(
                    JSON.stringify({item:productId,location:storeLocation[0].storeLocation})
                );
                const {loaders:{getProductsByIdLoader}} = context;
                const product = await getProductsByIdLoader.load(productId);
                const { loaders:{productsByCategory} } = context;
                productsByCategory.clear(product.category);
                getProductsByIdLoader.clear(productId);
            }

            return payment(paymentMethod, totalAmount, records, tokenId,storeLocation[0].storeLocation);
        },
        //Sample Record: { stockLocation: 'TSUEN_WAN', productId: 2, quantity: 10 }
        addStockRecord: async (_: any, { record }: IAddStockRecordInput,context:any) => {
            const {loaders:{getProductsByIdLoader}} = context;
            const product = await getProductsByIdLoader.load(record.productId);
            const { loaders:{productsByCategory} } = context;
            productsByCategory.clear(product.category);
            getProductsByIdLoader.clear(record.productId);
            const newStockRec =  await addStockRecord(record);

            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const {loaders:{sumOfStockQtyByProduct}} = context;
            const tempid = await getLatestStockIdsByLocationLoader.load(record.productId);
            if (tempid.length>0){
                sumOfStockQtyByProduct.clear(tempid[0].id);
            }

            //Refresh Stock Record
            getLatestStockIdsByLocationLoader.clear(record.productId);
            const histories = await getLatestStockIdsByLocationLoader.load(record.productId);
            const historiesByLocation = histories.filter((item:any)=>item.location === (newStockRec as any).location);
            const historiesByLocationById = historiesByLocation.filter((item:any)=>item.id === (newStockRec as any).id);
            return historiesByLocationById?historiesByLocationById[0]:null;
        },
        //addInventoryRecords: (_: any, { records }: IAddInventoryInput) => addInventoryRecord(records),
        addInventoryRecords: async (_: any, { records }: IAddInventoryInput,context:any) =>{
            //Clear Cache all of products
            const { loaders: { getLatestInventoriesIdsByProductAndLocationLoader } } = context;
            const {loaders:{getLatestStockIdsByLocationLoader}} = context;
            const {loaders:{sumOfStockQtyByProduct}} = context;
            const {loaders:{updatingProductRemainingQuantityLoader}} = context;
            for (const record of records) {
                getLatestInventoriesIdsByProductAndLocationLoader.clear(record.productId);

                const { loaders: { getProductsByIdLoader } } = context;
                const product = await getProductsByIdLoader.load(record.productId);
                const { loaders: { productsByCategory } } = context;
                productsByCategory.clear(product.category);
                getProductsByIdLoader.clear(record.productId);

                const tempid = await getLatestStockIdsByLocationLoader.load(record.productId);
                if (tempid.length>0){
                    sumOfStockQtyByProduct.clear(tempid[0].id);
                }
                
                const location = await getInventoryLocationId(record.storeLocation);
                updatingProductRemainingQuantityLoader.clear(
                    JSON.stringify({item:record.productId,location:location})
                );
            }

            await stockTake(records);
            const ids = await addInventoryRecord(records);
            //Loading new inventory histories
            const { loaders: { getInventoryByIdsLoader } } = context;
            const results = await getInventoryByIdsLoader.loadMany(ids);
            return results;
        },
        addItem:async(_:any,args:any,context:any)=>{
            /*Variables
            {
                item: { foodName: 'Product 1', category: 'DRINK', price: 1, unit: 'CUP' },
                image: Promise {
                    {
                    filename: '(盒裝)泰國固力果百力滋-香辣燒烤.jpg',
                    mimetype: 'image/jpeg',
                    encoding: '7bit',
                    createReadStream: [Function: createReadStream]
                    }
                }
            } 
            */
            /* Response
            {
                id: 'w3uPlSy4P',
                filename: '(盒裝)泰國固力果百力滋-香辣燒烤.jpg',
                mimetype: 'image/jpeg',
                path: './test/(盒裝)泰國固力果百力滋-香辣燒烤.jpg',
                uploadedFileName: '(盒裝)泰國固力果百力滋-香辣燒烤.jpg'
            } 
            */
            const {item,file} = args;
            const {loaders:{getCategoryIdsLoader}} = context;
            const categories = await getCategoryIdsLoader.load(item.category);
            const categoryId =  categories[0].id;

            const { loaders:{productsByCategory} } = context;
            productsByCategory.clear(categoryId);

            const {loaders:{getUnitIdsLoader}} = context;
            const units = await getUnitIdsLoader.load(item.unit);
            const unitId = units[0].id;
            return await addItem(item.foodName,categoryId,item.price,unitId,file);
        },
        testUpload: async(_:any,{file}:any)=>{
            //const UPLOAD_DIR = './test';
            //const fileMetadata:FileMetaData = await storeUpload(file,UPLOAD_DIR,1);
            //console.log(fileMetadata);
            return await getProductById(1);
        },
        addCashRecord: async(_:any,{record,totalAmount,tokenId}:any,context:any)=>{
            const {loaders:{inventoryLocationNameWithConvertionLoader}} = context;
            const storeLocation = await inventoryLocationNameWithConvertionLoader.load(record.storeLocation);
            return payment("CASH", totalAmount, [record],tokenId,storeLocation[0].storeLocation);            
        }
    }
}
export default resolvers;