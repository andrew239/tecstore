import knex from '../../tools/KnexDatabaseHelper';
import humps from 'humps';
import {extractValuesToArray,mapDB_RecordByKeyToArrayInOrder} from '../../tools/ramda';
import {storeUpload,FileMetaData} from '../common/graphqlUpload';
import DataLoader from 'dataloader';

export interface IProduct{
    id:number,
    foodName:string,
    category:number,
    price:number,
    unit:number,
    imageUrl:string,
    stock:Array<{}>,
    inventories:Array<{}>
}

export interface IProductInput{
    item: {
        foodName:string,
        category:string,
        price:number,
        unit:string
    },
    image:any //TODO: find the type!
}

export async function getCashRecordId(){
    const product = await knex.raw(`select min(id) as id from item`);
    return product[0].id;
}

export async function getProductById(id:number){
    const product = await knex.raw(`select * from item where id=${id}`);
    return humps.camelizeKeys(product[0]);
}

export async function getAllOfProducts(){
    const products = await knex.raw(`select * from item`);
    return humps.camelizeKeys(products);
}

export async function getCategoryName(id:number) {
    const category = await knex.raw(`select category from category where id=${id}`);
    return extractValuesToArray(category,"category")[0];
}

export function getCategoryIdsLoader(){
    return new DataLoader(getCategoryId)
}

export async function getCategoryId(names:any) {
    const categories = await knex.raw(`select * from category where category = ANY('{${names}}')`);
    return await mapDB_RecordByKeyToArrayInOrder(categories,names,"category");
}

export async function getUnitName(id:number) {
    const unit = await knex.raw(`select unit from unit where id=${id}`);
    return extractValuesToArray(unit,"unit")[0];
}

export function getUnitNameByIdsDataLoader() {
    return new DataLoader(getUnitNameByIds)
}

export async function getUnitNameByIds(ids:any) {
    const units = await knex.raw(`select id,unit from unit where id= ANY('{${ids}}')`);
    return await mapDB_RecordByKeyToArrayInOrder(units,ids,"id");
}

export function getUnitIdsLoader(){
    return new DataLoader(getUnitIds);
}

export async function getUnitIds(names:any) {
    const units = await knex.raw(`select id,unit from unit where unit= ANY('{${names}}')`);
    return await mapDB_RecordByKeyToArrayInOrder(units,names,"unit");
}

export async function productsByCategory(ids:any){
    //Work Example: select * from item where category= ANY('{1}');
    //DataLoader is testing here, the case don't need dataloader here actually.
    //const ids = await getCategoryIds(enums);
    const categories = await knex.raw(`select * from item where category = ANY('{${ids}}') and id > 1`);
    const mappedResult = await mapDB_RecordByKeyToArrayInOrder(categories,ids,"category");
    return humps.camelizeKeys(mappedResult);

    //For teaching purpose, I will show another handling without Ramda
    /*let mappedResultGroupBy:any = {};
    for(let row of categories){
        mappedResultGroupBy = {
            ...mappedResultGroupBy,
            [row.category]:mappedResultGroupBy[row.category]?[...mappedResultGroupBy[row.category],row]:[],
        }
    }
    const mappedResult2 = ids.map(id=>[...mappedResultGroupBy[id]]);
    return humps.camelizeKeys(mappedResult2);*/
}

export function productsByCategoryDataLoader() {
    return new DataLoader(productsByCategory);
}

export function getProductsByIdLoader() {
    return new DataLoader(getProductsById);
}

export async function getProductsById(ids:any) {
    return humps.camelizeKeys(await knex.raw(`select * from item where id = ANY('{${ids}}') and id > 1`));
}

export async function getAllOfProductsIds(){
    const minId = await knex.raw(`select min(id) as id from item`);
    const products = await knex.raw(`select id from item where id > ${minId[0].id}`);
    return extractValuesToArray( products,"id");
}

export function getProductCategoryNameLoader(){
    return new DataLoader(getProductCategoryName);
}

export async function getProductCategoryName(ids:any) {
    const categories = await knex.raw(`select id,category from category where id= ANY('{${ids}}')`);
    return  await mapDB_RecordByKeyToArrayInOrder(categories,ids,"id");
}

export function getProductFieldFromArray(data:any,field:string){
    return extractValuesToArray(data,field);
}

export async function addItem(foodName:string,category:number,price:number,unit:number,image:any){
    const UPLOAD_DIR = './upload';
    const fileMetadata:FileMetaData = await storeUpload(image,UPLOAD_DIR,1);
    const product = await knex.raw(`insert into item(food_name,category,price,unit,image_url) values ('${foodName}',${category},${price},${unit},'${fileMetadata.uploadedFileName}')
                                   returning id,food_name,category,price,unit,image_url`);
    return humps.camelizeKeys(product)[0];
}