import {productsByCategoryDataLoader,getProductsByIdLoader,
    getProductCategoryNameLoader,getUnitNameByIdsDataLoader,
    getCategoryIdsLoader,getUnitIdsLoader} from '../products/products';
import {stocksLocationNameLoader,
    stocksByIdsLoader,stocksByProductIdLoader,sumOfStockQtyByProductLoader,getLatestStockIdsByLocationLoader} from '../stock/stock';
import {inventoryByProductIdsLoader,inventoryLocationNameLoader,inventoryByIdsLoader,
    getProductByInventoryIdsLoader,getInventoryHistoriesByInventoryIdsLoader,
    getLatestInventoriesIdsByProductAndLocationLoader,inventoryLocationNameWithConvertionLoader,
    updatingProductRemainingQuantityLoader} from '../inventory/inventory';
import {transactionsByBillIdDataLoader} from '../transaction/transaction';

export const context = {
    loaders:{
        getProductsByIdLoader: getProductsByIdLoader(),
        productsByCategory: productsByCategoryDataLoader(),
        getProductCategoryNameLoader:getProductCategoryNameLoader(),
        getCategoryIdsLoader:getCategoryIdsLoader(),
        getUnitIdsLoader:getUnitIdsLoader(),
        getStocksByIds:stocksByIdsLoader(),
        getStocksByProductId:stocksByProductIdLoader(),
        sumOfStockQtyByProduct:sumOfStockQtyByProductLoader(),
        getStocksLocationNameLoader:stocksLocationNameLoader(),
        getLatestStockIdsByLocationLoader:getLatestStockIdsByLocationLoader(),
        getUnitNameByIdsDataLoader:getUnitNameByIdsDataLoader(),
        getInventoryByIdsLoader:inventoryByIdsLoader(),
        getInventoryHistoriesByInventoryIdsLoader:getInventoryHistoriesByInventoryIdsLoader(),
        getProductByInventoryIdsLoader: getProductByInventoryIdsLoader(),
        getInventoryByProductIdsLoader:inventoryByProductIdsLoader(),
        getInventoryLocationNameLoader:inventoryLocationNameLoader(),
        getLatestInventoriesIdsByProductAndLocationLoader:getLatestInventoriesIdsByProductAndLocationLoader(),
        inventoryLocationNameWithConvertionLoader:inventoryLocationNameWithConvertionLoader(),
        getTransactionsByBillIdDataLoader:transactionsByBillIdDataLoader(),
        updatingProductRemainingQuantityLoader: updatingProductRemainingQuantityLoader()
    }
}