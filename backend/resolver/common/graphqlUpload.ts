/*
References:
https://www.runoob.com/nodejs/nodejs-stream.html
https://stackoverflow.com/questions/49771811/postman-ordering-of-fields-in-form-data-request
https://www.youtube.com/watch?v=Ue3Tn0ZzfdY
 */
import { createWriteStream, unlink } from 'fs';
import dotenv from 'dotenv';
dotenv.config();
import shortid from 'shortid';
//import path from 'path';
import aws from 'aws-sdk';
import axios from "axios";

export type FileMetaData = {
    id:string
    filename:string
    mimetype:string
    path:string
    uploadedFileName:string
}

export const storeUpload = async (upload: any, uploadedPath: string,pid:number) => {
    const { createReadStream, filename, mimetype } = await upload;
    const stream = createReadStream();
    const id = shortid.generate();
    //const uploadedFileName = `${pid}${path.extname(filename)}`;
    const uploadedFileName = `${filename}`;
    const fPath = `${uploadedPath}/${uploadedFileName}`;
    const file:FileMetaData = { id, filename, mimetype, path:fPath, uploadedFileName };
    await new Promise((resolve, reject) => {
        const writeStream = createWriteStream(fPath);
        writeStream.on('finish', resolve);
        writeStream.on('error', (error) => {
            unlink(fPath, () => {
                reject(error);
            });
        });
        stream.on('error', (error: any) => writeStream.destroy(error));
        stream.pipe(writeStream);
    });

    const s3 = new aws.S3({
        signatureVersion: 'v4',
        region: 'ap-southeast-1',
    });

    const s3Params = {
        Bucket: process.env.S3_BUCKET+"/upload",
        Key: filename,
        Expires: 60,
        ContentType: mimetype,
        ACL: 'public-read',
      };
    const signedRequest = await s3.getSignedUrl('putObject', s3Params);
    //const url = `https://${s3Bucket}.s3.amazonaws.com/${filename}`;
    const options = {
        headers: {
          "Content-Type": mimetype
        }
     };
    await axios.put(signedRequest, file, options);
    return file;
};