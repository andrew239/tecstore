/*
References:
https://stackoverflow.com/questions/35787892/default-value-in-select-query-for-null-values-in-postgres
*/
import knex from '../../tools/KnexDatabaseHelper';
import {
    extractValuesToArray, mapDB_RecordByKeyToArrayInOrder,
    transformDateFromFlattenToNestedStructure, convertFromDBValueToEnum,
    convertEnumToDBValue, extractValuesMappingToObj, insertInBatchByString,groupDataBy,
    replaceStringByList
} from '../../tools/ramda';
//import humps from 'humps';
import { getAllStockLocation ,getStockLocationId} from '../stock/stock';
import DataLoader from 'dataloader';

export interface IAddInventoryInputMetadata {
    stockLocation: string,
    storeLocation: string,
    productId: number,
    quantity: number
}

export interface IAddInventoryInput {
    records: IAddInventoryInputMetadata[]
}

async function getAllInventoryLocation() {
    return await knex.raw(`select * from inventory_location`);
}

export async function getInventoryLocationNameByIds(ids: any) {
    const results = await knex.raw(`select * from inventory_location where id = ANY('{${ids}}')`);
    const resultsRaw = await mapDB_RecordByKeyToArrayInOrder(results, ids, "id");
    return convertFromDBValueToEnum(resultsRaw, "/", " ", "location");
}

export function inventoryLocationNameLoader() {
    return new DataLoader(getInventoryLocationNameByIds);
}

export async function getInventoryLocationId(name: string) {
    const location = replaceStringByList(name,[{ i: "_", r: " " }, { i: "_", r: " " }, { i: "f", r: "/F" }]);
    const result = await knex.raw(`select id from inventory_location where location = '${location}'`);
    return extractValuesToArray(result, "id")[0];
}

export function inventoryLocationNameWithConvertionLoader(){
    return new DataLoader(inventoryLocationNameWithConversion);
}

export async function inventoryLocationNameWithConversion(names:any) {
    const allInvLoc = await getAllInventoryLocation();
    const allInvLocInIds = extractValuesMappingToObj(allInvLoc);
    const data = names.map((name:string)=>({storeLocation:name, orgName:name}));
    const transformedDataStore = await convertEnumToDBValue(data,
        "storeLocation",
        [{ i: "_", r: " " }, { i: "_", r: " " }, { i: "f", r: "/F" }],
        allInvLocInIds);
    const f = await mapDB_RecordByKeyToArrayInOrder(transformedDataStore,names,"orgName");
    return f;
}

export async function addInventoryRecord(records: IAddInventoryInputMetadata[]) {
    const allInvLoc = await getAllInventoryLocation();
    const allInvLocInIds = extractValuesMappingToObj(allInvLoc);
    const transformedDataStore = await convertEnumToDBValue(records,
        "storeLocation",
        [{ i: "_", r: " " }, { i: "_", r: " " }, { i: "f", r: "/F" }],
        allInvLocInIds);

    const allStockLoc = await getAllStockLocation();
    const allStockLocIds = extractValuesMappingToObj(allStockLoc);
    const transformedData = await convertEnumToDBValue(transformedDataStore,
        "stockLocation",
        [{ i: "_", r: " " }],
        allStockLocIds);
    const finalData = insertInBatchByString(transformedData);
    // const productIds = await knex.raw(`insert into inventory (location,item,quantity) values ${finalData} returning item`);
    // const products = await knex.raw(`select * from item where id=ANY('{${extractValuesToArray(productIds,"item")}}')`);
    // return humps.camelizeKeys(products);
    const ids = await knex.raw(`insert into inventory (stock,location,item,quantity) values ${finalData} returning id`);
    return extractValuesToArray(ids, "id");
}

export function inventoryByProductIdsLoader() {
    return new DataLoader(getInventoryByProductIds);
}

const transformFunc = (id: any, innerItemsEachOuter: any, aggregationFunc: any, aggKey: any) => (
    {
        location: parseInt(id),
        quantity: aggregationFunc(aggKey, innerItemsEachOuter[id]),
        histories: innerItemsEachOuter[id]
    }
);

export async function getInventoryByProductIds(ids: any) {
    const results = await knex.raw(`select item,location,quantity from inventory where item = ANY('{${ids}}') and summarized = false`);
    const tranformedResults = await transformDateFromFlattenToNestedStructure(results, ids, "item", "location", transformFunc, "quantity");
    return tranformedResults;
}

export function inventoryByIdsLoader() {
    return new DataLoader(inventoryByIds);
}


export async function inventoryByIds(ids: any) {
    const rawProductIds = await knex.raw(`select distinct item from inventory where id=ANY('{${ids}}') and writeoff = false`);
    const productIds = extractValuesToArray(rawProductIds, "item");
    const results = await knex.raw(`select main.id as id,main.item as item, main.location as location,aggTable.quantity as quantity from (select * from inventory where id = ANY('{${ids}}') and writeoff = false) as main left outer join (select item,location,sum(quantity) as quantity from inventory where item = ANY('{${productIds}}') and writeoff = false group by location,item) as aggTable on main.item = aggTable.item and main.location = aggTable.location`);
    const f =  await mapDB_RecordByKeyToArrayInOrder(results,ids,"id");
    return f;
}

export function inventoryHistoriesByIdsLoader() {
    return new DataLoader(inventoryHistoriesByIds);
}

export async function inventoryHistoriesByIds(ids: any) {
    return [];
}

export function getProductByInventoryIdsLoader() {
    return new DataLoader(getProductByInventoryIds);
}

export async function getProductByInventoryIds(ids: any) {
    return await knex.raw(`select distinct item from inventory where id=ANY('{${ids}}') and writeoff = false`);
}

export function getInventoryHistoriesByInventoryIdsLoader(){
    return new DataLoader(getInventoryHistoriesByInventoryIds);
}

export async function getInventoryHistoriesByInventoryIds(ids:any) {
    const rawProductIds = await knex.raw(`select distinct item from inventory where id=ANY('{${ids}}') and summarized = false`);
    const productIds = extractValuesToArray(rawProductIds, "item");
    // const results = await knex.raw(`select item,stock,location,quantity from inventory where item = ANY('{${productIds}}');`);
    // console.log(results);
    // return [groupDataBy("location",results)];
    const results = await knex.raw(`select ref.id as primary_id, main.location as location, main.id as id,main.stock as stock,main.quantity as quantity, main.item as item from (select * from inventory where item=ANY('{${productIds}}') and summarized = false) as main inner join (select id,item,stock,location,quantity from inventory where id = ANY('{${ids}}') and summarized = false) as ref on main.location = ref.location and main.item = ref.item and main.stock = ref.stock`);
    return await mapDB_RecordByKeyToArrayInOrder(results,ids,"primary_id");
}

export function groupInventoryHistoriesByLocation(data:any) {
    return groupDataBy("location",data);
}

export async function getLatestInventoriesIdByProductAndLocation(productId:number) {
    const ids = await knex.raw(`select max(id) as id from inventory where item = ${productId} and writeoff = false group by item,location`);
    return  extractValuesToArray(ids,"id");
}

export function getLatestInventoriesIdsByProductAndLocationLoader(){
    return new DataLoader(getLatestInventoriesIdsByProductAndLocation);
}

export async function getLatestInventoriesIdsByProductAndLocation(productId:any) {//inv ids
    const ids = await knex.raw(`select item,max(id) as id from inventory where item = ANY('{${productId}}') and writeoff = false group by item,location`);
    return await mapDB_RecordByKeyToArrayInOrder(ids,productId,"item");
}

export function getInventoryFieldFromArray(data:any,field:string){
    return extractValuesToArray(data,field);
}

export function updatingProductRemainingQuantityLoader(){
    return new DataLoader(updatingProductRemainingQuantity);
}

export async function updatingProductRemainingQuantity(_inventoryHistory:any) {
    const inventoryHistory = _inventoryHistory.map((d:any)=>JSON.parse(d));
    const productIds = extractValuesToArray(inventoryHistory, "item");
    const results = await knex.raw(`select input.item,input.location,input.quantity as input,cast(coalesce(sold.sold_qty,0) as integer) as sold, input.quantity - cast(coalesce(sold.sold_qty,0) as integer) as remain  from (select item,location,sum(quantity) as quantity from inventory where item = ANY('{${productIds}}') and writeoff = false group by location,item) as input left outer join (select item,inventory,sum(quantity) as sold_qty from transaction where item = ANY('{${productIds}}') and writeoff = false group by item,inventory) as sold on input.item = sold.item and input.location = sold.inventory`);
    let finalResults:any= [];
    for(const elementsByLocation of inventoryHistory){
        const resultsByItemAndLocation = await groupDataBy("location",results.filter((x:any)=>
        x.item ===(elementsByLocation as any).item 
        && x.location === (elementsByLocation as any).location ));
        finalResults = [ ...finalResults as any,
            {
                [ JSON.stringify({item:(elementsByLocation as any).item,location:(elementsByLocation as any).location} as any)]:
                resultsByItemAndLocation[(elementsByLocation as any).location][0]
            }
        ];
    }
    return finalResults;
}

export async function stockTake(records:IAddInventoryInputMetadata[]) {
     /*
      1. select id from inventory where item = 2 and location= 1 and writeoff = false;
      2. select sum(quantity) from inventory where id = ANY('{305,318,319,320,321,322}'); //Current QTY
      3. select id from transaction where item = 2 and inventory = 2 and writeoff = false;
      4. select sum(quantity) from transaction where id = ANY('{58,59,60,61}'); //Current sold QTY
      5. insert into inventory (stock,location,item,quantity,summarized,writeoff) values (2,1,2,10,false,false) returning id;
      6. update inventory set writeoff = true where id = ANY('{305,318,319,320,321,322}');
      7. insert into inventory  (stock,location,item,quantity,summarized,writeoff) values (2,1,2,4,true,false) returning id;
      8. update transaction set writeoff = true where id = ANY('{58,59,60,61}');
      */
    const itemAndLocationSets = records.map((set:IAddInventoryInputMetadata)=>({
        item:set.productId,
        location:set.storeLocation,
        stockLocation:set.stockLocation
    }));

    for(const set of itemAndLocationSets){
        const invLocationId = await getInventoryLocationId(set.location);
        const inventoryIds = extractValuesToArray(await knex.raw(`select id from inventory where item = ${set.item} and location= ${invLocationId} and writeoff = false`),"id");
        const qty = extractValuesToArray(await knex.raw(`select cast(coalesce(sum(quantity),0) as integer) as sum from inventory where id = ANY('{${inventoryIds}}')`),"sum");
        const transactionIds = extractValuesToArray(await knex.raw(`select id from transaction where item = ${set.item} and inventory = ${invLocationId} and writeoff = false`),"id");
        const soldQty = extractValuesToArray(await knex.raw(`select cast(coalesce(sum(quantity),0) as integer) as sum from transaction where id = ANY('{${transactionIds}}')`),"sum");

        await knex.raw(`update inventory set writeoff = true where id = ANY('{${inventoryIds}}');`);
        const stockLocationId = await getStockLocationId(set.stockLocation);
        await knex.raw(`insert into inventory (stock,location,item,quantity,summarized,writeoff) values (${stockLocationId},${invLocationId},${set.item},${qty[0] - soldQty[0]},true,false)`);
        await knex.raw(`update transaction set writeoff = true where id = ANY('{${transactionIds}}')`);
    }
}