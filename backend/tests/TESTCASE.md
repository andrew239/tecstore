```
{
  product(id:2){
    stock{
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
  }
}
```

```
{
  products{
    id
    stocks{
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
  }
}

{
  products{
    id
    foodName
    stocks{
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
    inventories{
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
  }
}

{
  products{
    id
    foodName
    stocks{
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
    inventories{
      location
      quantity
      remainQuantity
      histories{
        location
        addedQuantity
      }
    }
  }
}
```

```
mutation($record:AddStockRecordInput!){
           addStockRecord(record:$record){
    location
    quantity
           }
       }
       {
           "record": {
               "stockLocation": "TSUEN_WAN",
               "productId": 2,
               "quantity": 10
           }
       }
```

```
mutation ($records: [AddInventoryRecordInput!]!) {
  addInventoryRecords(records: $records) {
    location
    quantity
    histories{
      stock{
        location
        quantity
        histories{
          location
          addedQuantity
        }
      }
      location
      addedQuantity
    }
  }
}

       {
        "records": [
            {
              "stockLocation": "TSUEN_WAN",
            "storeLocation": "TSUEN_WAN_27F",
            "productId": 2,
            "quantity": 1
            },
            {
              "stockLocation": "TSUEN_WAN",
            "storeLocation": "TSUEN_WAN_27F",
            "productId": 6,
            "quantity": 2
            },
            {
              "stockLocation": "TSUEN_WAN",
            "storeLocation": "TSUEN_WAN_5F",
            "productId": 3,
            "quantity": 5
            },
            {
              "stockLocation": "SHEUNG_WAN",
            "storeLocation": "SHEUNG_WAN",
            "productId": 10,
            "quantity": 6
            }
        ]
        }
```

```
       mutation($records:[AddPaymentRecordInput!]!,$totalAmount:Float!,$paymentMethod:PAYMENT_METHOD!,$tokenId:String!){
  payment(records:$records,totalAmount:$totalAmount,paymentMethod:$paymentMethod,tokenId:$tokenId){
    id
    refNo
    approval
    date
    records{
      id
      product{
        id
        foodName
        category
        price
        unit
        imageUrl
        stocks{
          location
          quantity
          histories{
            location
            addedQuantity
          }
        }
        inventories{
          location
          quantity
          histories{
            location
            addedQuantity
          }
        }
      }
      quantity
      amount
    }
  }
}

{
  "records": [
    {
      "storeLocation": "TSUEN_WAN_27F",
      "productId": 2,
      "quantity": 2
    },
    {
      "storeLocation": "TSUEN_WAN_27F",
      "productId": 3,
      "quantity": 1
    },
    {
      "storeLocation": "TSUEN_WAN_27F",
      "productId": 4,
      "quantity": 3
    }
  ],
  "totalAmount": 1000,
  "paymentMethod": "CREDIT_CARD",
  "tokenId": "tok_1I15HVFiPP4ykMuwSxolgdk2"
}
```


```
query($category:CATEGORY!,$location:STORE_LOCATION!){
  productsByCategory(category:$category){
    id
    foodName
    category
    inventory(location:$location){
      location
      quantity
      remainQuantity
      product{
        id
        foodName
      }
      histories{
        stocks{
          location
          quantity
        }
        location
        addedQuantity
      }
    }
  }
}

{
  "category": "DRINK",
  "location": "TSUEN_WAN_27F"
}
```

```
query($category:CATEGORY!,$storeLocation:STORE_LOCATION!,$stockLocation:STOCK_LOCATION!){
  productsByCategory(category:$category){
    id
    foodName
    category
    inventory(location:$storeLocation){
      location
      quantity
      remainQuantity
      product{
        id
        foodName
      }
      histories{
        stock(location:$stockLocation){
          location
          quantity
          histories{
            location
            addedQuantity
          }
        }
        location
        addedQuantity
      }
    }
  }
}
{
  "category": "DRINK",
  "storeLocation": "TSUEN_WAN_27F",
  "stockLocation": "TSUEN_WAN"
}
```

```
query($category:CATEGORY!,$storeLocation:STORE_LOCATION!,$stockLocation:STOCK_LOCATION!){
  productsByCategory(category:$category){
    id
    foodName
    category
    stock(location:$stockLocation){
      location
      quantity
      histories{
        location
        addedQuantity
      }
    }
    inventory(location:$storeLocation){
      location
      quantity
      remainQuantity
      product{
        id
        foodName
      }
      histories{
        stock(location:$stockLocation){
          location
          quantity
          histories{
            location
            addedQuantity
          }
        }
        location
        addedQuantity
      }
    }
  }
}
{
  "category": "DRINK",
  "storeLocation": "TSUEN_WAN_27F",
  "stockLocation": "SHEUNG_WAN"
}
```

```
query ($inventoryLocation: STORE_LOCATION!) {
  products {
    id
    category
    foodName
    imageUrl
    price
    unit
    inventory(location: $inventoryLocation) {
      quantity
      remainQuantity
      histories {
        location
        addedQuantity
        stock {
          location
          quantity
        }
      }
    }
  }
}

{
  "inventoryLocation": "TSUEN_WAN_27F"
}
```