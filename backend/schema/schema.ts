//import { buildSchema } from "graphql"

const schema = /*buildSchema(*/`
    scalar Date
    scalar Upload

    schema{
        query:Query
        mutation:Mutation
    }

    enum CATEGORY{
        DRINK
        SNACK
        NOODLES
    }

    enum STOCK_LOCATION{
        TSUEN_WAN
        SHEUNG_WAN
    }

    enum STORE_LOCATION{
        TSUEN_WAN_27F
        TSUEN_WAN_5F
        SHEUNG_WAN
    }

    enum UNIT{
        CAN
        PACK
        BOTTLE
        CUP
        BAG
    }

    enum PAYMENT_METHOD{
        CREDIT_CARD
        PAYME
        CASH
    }

    enum APPROVAL_RESULT{
        APPROVED
        DECLINED
    }

    type Query{
        product(id:ID!):Product
        products:[Product]
        productsByCategory(category:CATEGORY!):[Product]
        salesVolumes(range:DateRange!,productId:ID = -1):[SalesVolume]!
        paymentRecords(range:DateRange!):[Bill]!
    }

    # A type that describe the item (food)
    type Product{
        id:ID!
        foodName:String!
        category:String!
        price:Float!
        unit:String!
        imageUrl:String!
        stocks:[Stock]
        stock(location:STOCK_LOCATION):[Stock]
        inventories:[Inventory]
        inventory(location:STORE_LOCATION):Inventory
    }

    # A type that describe the each stock(store) information of a food
    type Stock{
        id:ID!
        location:String!
        quantity:Int!
        histories:[StockHistory]
    }

    # A type that describe the each inventory (move item from stock for selling) information of a food
    type Inventory{
        product:Product!
        location:String!
        quantity:Int!
        remainQuantity:Int!
        histories:[InventoryHistory]
    }

    # A type that describe all of stock move-in record
    type StockHistory{
        location:String!
        addedQuantity:Int!
    }

    # A type that describe all of stock move-out record
    type InventoryHistory{
        stocks:[Stock]
        stock:[Stock]
        location:String!
        addedQuantity:Int!
    }

    # To be implemented in the future
    type SalesVolume{
        product:Product!
        volume:Int!
    }

    # A type that describe each bill record
    type Bill{
        id:ID!
        totalPrice:Float!
        paymentMethod:String!
        creditCardNo:String!
        date:Date!
        refNo:String
        approval:String!
        records:[Transaction]!
    }

    # A type that describe each item brought record
    type Transaction{
        id:ID!
        product:Product!
        quantity:Int!
        amount:Float!
    }

    type Mutation{
        # Payment Processing such as Stripe, HSBC Payme Code
        payment(records:[AddPaymentRecordInput!]!, totalAmount:Float! ,paymentMethod:PAYMENT_METHOD!, tokenId:String!):Bill
        # Add Stock Move-In Record
        addStockRecord(record:AddStockRecordInput!):Stock
        # Add Stock Move-Out Record
        addInventoryRecords(records:[AddInventoryRecordInput!]!):[Inventory!]!
        # Add Item (Food)
        addItem(item:ProductInput,file:Upload):Product!
        # Testing GraphQL Upload File Function
        testUpload(file:Upload!):Product!
        # Add the sum of cash payment Record
        addCashRecord(record:AddPaymentRecordInput!,totalAmount:Float!, tokenId:String!):Bill
    }

    input DateRange{
        start:Date!
        end:Date
    }

    input AddPaymentRecordInput{
        storeLocation:STORE_LOCATION
        productId:Int! = 1
        quantity:Int! = 0
    }

    input AddStockRecordInput{
        stockLocation:STOCK_LOCATION
        productId:Int!
        quantity:Int!
    }

    input AddInventoryRecordInput{
        stockLocation:STOCK_LOCATION
        storeLocation:STORE_LOCATION
        productId:Int!
        quantity:Int!
    }

    input ProductInput{
        foodName:String
        category:CATEGORY!
        price:Float!
        unit:UNIT!
    }
`;//);
export default schema;
