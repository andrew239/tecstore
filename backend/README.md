# GraphQL Server

## Installation
Server Part
```
yarn init -y
yarn add ts-node typescript @types/node
touch tsconfig.json
touch index.js
touch app.ts
yarn add express @types/express body-parser @types/body-parser dotenv @types/dotenv
yarn add ramda @types/ramda
yarn add change-case
yarn add moment @types/moment
```

Stripe Setup
```
yarn add stripe @types/stripe
```

Database Setup
```
yarn add knex pg @types/knex @types/pg
yarn knex init -x ts
touch .env
yarn add humps @types/humps
```

SQL statement involved
```
CREATE DATABASE tecstore;
```

Knex Migration will follow below ERD building the database
![alt "ERD Diagram"](./diagram/TecStore.png) 

```
yarn knex migrate:make --knexfile knexfile.ts -x ts create-tecstore
yarn knex migrate:latest
yarn knex seed:make -x ts create-tecstore-data 
```

Knex Debug Mode - for teaching purpose
```
export DEBUG=knex:query
```

Only 3 SQL queries are called if dataloader is used.
```
knex:query select * from item undefined +0ms
knex:query select item,location,quantity from stock where item = ANY('{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57}') undefined +21ms
knex:query select * from stock_location where id = ANY('{1,2}') undefined +30ms
```

20 SQL queries ara called if dataloader is NOT used.
```
knex:query select * from item undefined +0ms
  knex:query select item,location,quantity from stock where item = ANY('{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57}') undefined +62ms
  knex:query select location from stock_location where id = 1 undefined +46ms
  knex:query select location from stock_location where id = 1 undefined +98ms
  knex:query select location from stock_location where id = 1 undefined +2ms
  knex:query select location from stock_location where id = 2 undefined +0ms
  knex:query select location from stock_location where id = 2 undefined +1ms
  knex:query select location from stock_location where id = 2 undefined +0ms
  knex:query select location from stock_location where id = 2 undefined +1ms
  knex:query select location from stock_location where id = 2 undefined +0ms
  knex:query select location from stock_location where id = 2 undefined +0ms
  knex:query select location from stock_location where id = 2 undefined +0ms
  knex:query select location from stock_location where id = 2 undefined +1ms
  knex:query select location from stock_location where id = 2 undefined +1ms
  knex:query select location from stock_location where id = 2 undefined +1ms
  knex:query select location from stock_location where id = 2 undefined +2ms
  knex:query select location from stock_location where id = 1 undefined +10ms
  knex:query select location from stock_location where id = 1 undefined +5ms
  knex:query select location from stock_location where id = 2 undefined +2ms
  knex:query select location from stock_location where id = 2 undefined +3ms
```

GraphQL Part
```
yarn add install express-graphql @types/express-graphql graphql @types/graphql graphql-upload @types/graphql-upload
mkdir schema
cd schema && touch schema.ts && cd ..
mkdir resolver
cd resolver && touch resolver.ts
```
Next, the schema is defined first.

GraphQL Upload Field Type
```
  yarn add graphql-upload @types/graphql-upload
  yarn add shortid @ytypes/shortid
  yarn add shortid
  yarn add @types/shortid
```

Alternative Option of GraphQL
https://github.com/prisma-labs/graphql-yoga

Production EC2 Setup
```
export NODE_OPTIONS=--max_old_space_size=4096

sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=4096
sudo /sbin/mkswap /var/swap.1
sudo chmod 600 /var/swap.1
sudo /sbin/swapon /var/swap.1
sudo nano /etc/fstab -> /var/swap.1   swap    swap    defaults        0   0
sudo swapon -a
cat /proc/meminfo

https://medium.com/@evgeni.leonti/detect-heap-overflow-on-node-js-javascript-heap-out-of-memory-41cb649c6b33
https://stackoverflow.com/questions/38824440/how-to-set-memory-limit-to-nodejs-running-with-forever
https://stackoverflow.com/questions/17173972/how-do-you-add-swap-to-an-ec2-instance
```

References
```
https://dev.to/augani/how-to-build-a-graphql-api-from-scratch-19c0
http://blog.ma.beibeilab.com/ignore-tracked-files/
https://stackoverflow.com/questions/21292677/knex-js-debug-only-sql
```